
import UIKit
import AVFoundation
import Photos
import SafariServices
import FTIndicator

@objc protocol AVCapturePhotoOutputType {
    @available(iOS 10.0, *)
    var isLensStabilizationDuringBracketedCaptureSupported: Bool {get}
    @available(iOS 10.0, *)
    @objc(availableRawPhotoPixelFormatTypes)
    var __availableRawPhotoPixelFormatTypes: [NSNumber] {get}
    @available(iOS 10.0, *)
    var isHighResolutionCaptureEnabled: Bool {get @objc(setHighResolutionCaptureEnabled:) set}
    @available(iOS 10.0, *)
    @objc(supportedFlashModes)
    var __supportedFlashModes: [NSNumber] {get}
    @available(iOS 10.0, *)
    @objc(connectionWithMediaType:)
    func connection(with mediaType: AVMediaType) -> AVCaptureConnection?
    @available(iOS 10.0, *)
    @objc(capturePhotoWithSettings:delegate:)
    func capturePhoto(with settings: AVCapturePhotoSettings, delegate: AVCapturePhotoCaptureDelegate)
}

extension AVCaptureDevice.DiscoverySession: AVCaptureDeviceDiscoverySessionType {}
private protocol AVCaptureDeviceDiscoverySessionType: class {
    @available(iOS 10.0, *)
    var devices: [AVCaptureDevice] { get }
}

private enum AVCamManualSetupResult: Int {
    case success
    case cameraNotAuthorized
    case sessionConfigurationFailed
}

class CameraViewController: UIViewController , AVCaptureVideoDataOutputSampleBufferDelegate {
   

    //MARK:- Outlets
    @IBOutlet weak var previewView: AVCamManualPreviewView!
    @IBOutlet weak var previewHeight: NSLayoutConstraint!
    @IBOutlet weak var previewWidth : NSLayoutConstraint!
    @IBOutlet weak var cameraView: UIImageView!
    
    @IBOutlet weak var testImg_one: UIImageView!
    @IBOutlet weak var testImg_two: UIImageView!
    //MARK:- Variables
    var previewLayer: AVCaptureVideoPreviewLayer!
    var cameraLayer: AVCaptureVideoPreviewLayer!
    var timer = Timer()
    
    var outputFileLocation: URL!
    var videoWriterInput: AVAssetWriterInput?
    var videoWriter: AVAssetWriter?
    var audioWriterInput: AVAssetWriterInput?
    var isRecording:Bool = false
    var sessionAtSourceTime:CMTime?
    let videoOutput = AVCaptureVideoDataOutput()
    
    // Session management.
    @objc dynamic var session: AVCaptureSession!
    @objc dynamic var videoDeviceInput: AVCaptureDeviceInput?
    private var videoDeviceDiscoverySession: AVCaptureDeviceDiscoverySessionType?
    @objc dynamic var videoDevice: AVCaptureDevice?
    @objc dynamic var movieFileOutput: AVCaptureMovieFileOutput?
    @objc dynamic var photoOutput: AVCapturePhotoOutputType?
    @objc dynamic var stillImageOutput: AVCaptureStillImageOutput? //### iOS < 10.0
   
    // Utilities.
    private var setupResult: AVCamManualSetupResult = .success
    private var isSessionRunning: Bool = false
    private var backgroundRecordingID: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    private let kExposureDurationPower = 5.0 // Higher numbers will give the slider more sensitivity at shorter durations
    private let kExposureMinimumDuration = 1.0/1000 // Limit exposure duration to a useful range
    
    var camera = AVCaptureDevice.default(for: AVMediaType.video)
      var isovalue = Float()
      var temp_    = Float()
      var tint_    = Float()
      var fps_     = Int ()
      var exposure = Float()
      var focus    = Float()
      var zoom     =  Int()
      var sizeValue = "hd1920x1080"
     var border : CGFloat = 60
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let openCVWrapper = OpenCVWrapper()
        openCVWrapper.isThisWorking()
        
        
        setupValues()
        self.session = AVCaptureSession()
        // Create a device discovery session
        if #available(iOS 10.0, *) {
            let deviceTypes: [AVCaptureDevice.DeviceType] = [AVCaptureDevice.DeviceType.builtInWideAngleCamera, AVCaptureDevice.DeviceType.builtInDuoCamera, AVCaptureDevice.DeviceType.builtInTelephotoCamera]
            self.videoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: deviceTypes, mediaType: AVMediaType.video, position: .unspecified)
        }
        
        // Setup the preview view.
        previewLayer = AVCaptureVideoPreviewLayer(session: self.session)
        previewLayer.frame = previewView.bounds
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        previewView.layer.addSublayer(previewLayer)
        
        // Communicate with the session and other session objects on this queue.
        
        self.setupResult = .success
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            // The user has previously granted access to the camera.
            break
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) {granted in
                if !granted {
                    self.setupResult = .cameraNotAuthorized
                }
            }
        default:
            // The user has previously denied access.
            self.setupResult = .cameraNotAuthorized
        }
        self.configureSession()
        self.setUpWriter()
        
        if UserDefaults.standard.string(forKey: "log") == "On"{
            self.startRecording()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        switch self.setupResult {
        case .success:
            // Only setup observers and start the session running if setup succeeded.
            // self.session.startRunning()
            self.isSessionRunning = self.session.isRunning
        //self.startCapture()
        case .cameraNotAuthorized:
            DispatchQueue.main.async {
                let message = NSLocalizedString("BrightCodes doesn't have permission to use the camera, please change privacy settings", comment: "Alert message when the user has denied access to the camera" )
                let alertController = UIAlertController(title: "BrightCodes", message: message, preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil)
                alertController.addAction(cancelAction)
                // Provide quick access to Settings.
                let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"), style: .default) {action in
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                    } else {
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }
                }
                alertController.addAction(settingsAction)
                self.present(alertController, animated: true, completion: nil)
            }
        case .sessionConfigurationFailed:
            DispatchQueue.main.async {
                let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
                let alertController = UIAlertController(title: "BrightCodes", message: message, preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        //start timer
        timer = Timer.scheduledTimer(timeInterval: 10, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if self.setupResult == .success {
            self.session.stopRunning()
        }
        super.viewDidDisappear(animated)
    }
    
    
    //MARK:- Custom Methods
    func setupValues(){
        
        temp_ = 5300
        
        if UserDefaults.standard.value(forKey: "whiteBalance") != nil{
            temp_ = UserDefaults.standard.float(forKey: "whiteBalance")
            if temp_ == 0 {
                temp_ = 5300
            }
        }
        if UserDefaults.standard.value(forKey: "iso") != nil{
            isovalue = UserDefaults.standard.float(forKey: "iso")
        }
        else{
            isovalue = 100
        }
        
        if UserDefaults.standard.value(forKey: "focus") != nil{
            focus = UserDefaults.standard.float(forKey: "focus")
        }
        else{
            focus = 0.2
        }
        
        if UserDefaults.standard.value(forKey: "fps") != nil{
            fps_ = UserDefaults.standard.integer(forKey: "fps")
        }
        else{
            fps_ = 30
        }
        
        if UserDefaults.standard.value(forKey: "zoom") != nil{
            zoom = UserDefaults.standard.integer(forKey: "zoom")
        }
        else{
            zoom = 2
        }
        
        if UserDefaults.standard.value(forKey: "sizeKey") != nil{
            sizeValue = UserDefaults.standard.string(forKey: "sizeKey")!
        }
    }
    
    func chanfeFocus(){
        
        NSLog("\(self.camera?.formats)")
        
        do {
            
            try self.camera!.lockForConfiguration()
            
            self.camera?.activeFormat = (self.camera?.formats.last)!
            self.camera!.setFocusModeLocked(lensPosition: focus, completionHandler: nil)
            if zoom == 2{
                self.camera?.videoZoomFactor = 2
            }
            if zoom == 1 {
                self.camera?.videoZoomFactor = 1
            }
            else {
                self.camera?.videoZoomFactor = 1.5
            }
            
            //            else{
            //                if #available(iOS 11.0, *) {
            //                    self.camera?.videoZoomFactor = (self.camera?.minAvailableVideoZoomFactor)!
            //                } else {
            //                    // Fallback on earlier versions
            //                   // self.camera?.videoZoomFactor = 1
            //
            //                    self.camera?.videoZoomFactor = .leastNormalMagnitude
            //                }
            //            }
            
            self.camera!.unlockForConfiguration()
        } catch let error {
            NSLog("Could not lock device for configuration: \(error)")
        }
    }
    
    
    func changedurationandiso(){
        
        do {
            try self.camera!.lockForConfiguration()
            var isoValue_ = isovalue
            if isovalue <  (camera?.activeFormat.minISO)!{
                isoValue_ = (camera?.activeFormat.minISO)!
            }
            else if isovalue > (camera?.activeFormat.maxISO)!{
                isoValue_ = (camera?.activeFormat.maxISO)!
            }
            if UserDefaults.standard.value(forKey: "exposure") != nil{
                exposure = UserDefaults.standard.float(forKey: "exposure")
            }
            else{
                exposure = 0.40
            }
            
            let p = pow(Double(exposure), kExposureDurationPower) // Apply power function to expand slider's low-end range
            let minDurationSeconds = max(CMTimeGetSeconds(self.camera!.activeFormat.minExposureDuration), kExposureMinimumDuration)
            let maxDurationSeconds = CMTimeGetSeconds(self.camera!.activeFormat.maxExposureDuration)
            let newDurationSeconds = p * ( maxDurationSeconds - minDurationSeconds ) + minDurationSeconds; // Scale from 0-1 slider range to actual duration
            self.camera!.setExposureModeCustom(duration: CMTimeMakeWithSeconds(newDurationSeconds, 1000*1000*1000), iso: isoValue_, completionHandler: nil)
            self.camera!.unlockForConfiguration()
            
        } catch let error {
            NSLog("Could not lock device for configuration: \(error)")
        }
    }
    
    private func normalizedGains(_ gains: AVCaptureDevice.WhiteBalanceGains) -> AVCaptureDevice.WhiteBalanceGains {
        var g = gains
        g.redGain = max(1.0, g.redGain)
        g.greenGain = max(1.0, g.greenGain)
        g.blueGain = max(1.0, g.blueGain)
        
        g.redGain = min(self.camera!.maxWhiteBalanceGain, g.redGain)
        g.greenGain = min(self.camera!.maxWhiteBalanceGain, g.greenGain)
        g.blueGain = min(self.camera!.maxWhiteBalanceGain, g.blueGain)
        
        return g
    }
    
    //MARK:- UIButton Actions
    @IBAction func backAc(_ sender: Any) {
        if UserDefaults.standard.string(forKey: "log") == "On"{
            self.stopRecording()
         }
        else{
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    //MARK:- Helpers
    func showLoader(strForMessage:String!)
    {
        FTIndicator.showProgress(withMessage: strForMessage)
    }
    func dismissLoader()
    {
        FTIndicator.dismissProgress()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        let deviceOrientation = UIDevice.current.orientation
        
        if UIDeviceOrientationIsPortrait(deviceOrientation) || UIDeviceOrientationIsLandscape(deviceOrientation) {
            let previewLayer = self.previewView.layer as! AVCaptureVideoPreviewLayer
            previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation(rawValue: deviceOrientation.rawValue)!
        }
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }
    
    //MARK:- Session Management
    
    // Should be called on the session queue
    
    func configSize(){
        
         if sizeValue == "high"{
            self.session.sessionPreset = AVCaptureSession.Preset.high
            previewHeight.constant = self.view.frame.size.height - (40 + 65)
            previewWidth.constant = self.view.frame.size.width - 40
            border = 40
        }
        else if sizeValue == "medium"{
            self.session.sessionPreset = AVCaptureSession.Preset.medium
            previewHeight.constant = self.view.frame.size.height - (100 + 65)
            previewWidth.constant = self.view.frame.size.width - 100
            border = 100
        }
        else if sizeValue == "low"{
            self.session.sessionPreset = AVCaptureSession.Preset.low
            previewHeight.constant = self.view.frame.size.height - (120 + 65)
            previewWidth.constant = self.view.frame.size.width - 120
            border = 120
        }
        else if sizeValue == "cif352x288"{
            self.session.sessionPreset = AVCaptureSession.Preset.cif352x288
            previewHeight.constant = self.view.frame.size.height - (120 + 65)
            previewWidth.constant = self.view.frame.size.width - 120
            border = 120
        }
        else if sizeValue == "vga640x480"{
            self.session.sessionPreset = AVCaptureSession.Preset.vga640x480
            previewHeight.constant = self.view.frame.size.height - (100 + 65)
            previewWidth.constant = self.view.frame.size.width - 100
            border = 100
        }
        else if sizeValue == "hd1280x720"{
            self.session.sessionPreset = AVCaptureSession.Preset.hd1280x720
            previewHeight.constant = self.view.frame.size.height - (90 + 65)
            previewWidth.constant = self.view.frame.size.width - 90
            border = 90
        }
        else if sizeValue == "hd1920x1080"{
            self.session.sessionPreset = AVCaptureSession.Preset.hd1920x1080
            previewHeight.constant = self.view.bounds.size.height - (60 + 65)
            previewWidth.constant = self.view.bounds.size.width - 60
            border = 60
        }
        else if sizeValue == "hd4K3840x2160"{
            self.session.sessionPreset = AVCaptureSession.Preset.hd4K3840x2160
            previewHeight.constant = self.view.frame.size.height - (40 + 65)
            previewWidth.constant = self.view.frame.size.width - 40
            border = 40
        }
        else if sizeValue == "iFrame960x540"{
            self.session.sessionPreset = AVCaptureSession.Preset.iFrame960x540
            previewHeight.constant = self.view.frame.size.height - (90 + 65)
            previewWidth.constant = self.view.frame.size.width - 90
            border = 90
        }
        else if sizeValue == "iFrame1280x720"{
            self.session.sessionPreset = AVCaptureSession.Preset.iFrame1280x720
            previewHeight.constant = self.view.frame.size.height - (100 + 65)
            previewWidth.constant = self.view.frame.size.width - 100
            border = 100
        }
        cameraView.layoutIfNeeded()
    }
    
    private func configureSession() {
        guard self.setupResult == .success else {
            return
        }
        
        self.session.beginConfiguration()
        
        changedurationandiso()
        configSize()
        if (camera?.isWhiteBalanceModeSupported(.locked))! {
            
            if try! camera?.lockForConfiguration() != nil {
                camera?.whiteBalanceMode = .locked
                do {
                    
                    let temperatureAndTintValues = AVCaptureDevice.WhiteBalanceTemperatureAndTintValues(temperature: temp_, tint:25)
                    
                    let gain = camera?.deviceWhiteBalanceGains(for: temperatureAndTintValues)
                    
                    try camera!.lockForConfiguration()
                    let normalizedgains = normalizedGains(gain!) // Conversion can yield out-of-bound values, cap to limits
                    camera!.setWhiteBalanceModeLocked(with: normalizedgains, completionHandler: nil)
                    
                    camera!.activeVideoMinFrameDuration = CMTimeMake(Int64(fps_), 60)
                    camera!.activeVideoMaxFrameDuration = CMTimeMake(Int64(fps_), 60)
                    camera!.unlockForConfiguration()
                } catch let error {
                    NSLog("Could not lock device for configuration: \(error)")
                }
            }
            else{
                print("lock configured")
            }
            
            
        }
        
        // Add video input
        if #available(iOS 10.0, *) {
            videoDevice = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for:AVMediaType.video, position: .unspecified)
        } else {
            videoDevice = CameraViewController.deviceWithMediaType(AVMediaType.video.rawValue, preferringPosition: .back)
        }
        
        let videoDeviceInput: AVCaptureDeviceInput
        do {
            videoDeviceInput = try AVCaptureDeviceInput(device:camera!)
        } catch {
            NSLog("Could not create video device input: \(error)")
            self.setupResult = .sessionConfigurationFailed
            self.session.commitConfiguration()
            return
        }
        
        if self.session.canAddInput(videoDeviceInput) {
            self.session.addInput(videoDeviceInput)
            self.videoDeviceInput = videoDeviceInput
            
            DispatchQueue.main.async {
                
                let statusBarOrientation = UIApplication.shared.statusBarOrientation
                var initialVideoOrientation = AVCaptureVideoOrientation.portrait
                if statusBarOrientation != UIInterfaceOrientation.unknown {
                    initialVideoOrientation = AVCaptureVideoOrientation(rawValue: statusBarOrientation.rawValue)!
                }
                
                let previewLayer = self.previewView.layer as! AVCaptureVideoPreviewLayer
                previewLayer.connection?.videoOrientation = initialVideoOrientation
            }
        } else {
            NSLog("Could not add video device input to the session")
            self.setupResult = .sessionConfigurationFailed
            self.session.commitConfiguration()
            return
        }
        
        // Add audio input
        let audioDevice = AVCaptureDevice.default(for: AVMediaType.audio)
        let audioDeviceInput: AVCaptureDeviceInput!
        do {
            audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice!)
        } catch let error {
            audioDeviceInput = nil
            NSLog("Could not create audio device input: \(error)")
        }
        if self.session.canAddInput(audioDeviceInput) {
            self.session.addInput(audioDeviceInput)
        } else {
            NSLog("Could not add audio device input to the session")
        }
        
        /*     let movieFileOutput = AVCaptureMovieFileOutput()
         if self.session.canAddOutput(movieFileOutput) {
         self.session.addOutput(movieFileOutput)
         if let connection = movieFileOutput.connection(with: AVMediaType.video), connection.isVideoStabilizationSupported {
         connection.preferredVideoStabilizationMode = .auto
         }
         self.movieFileOutput = movieFileOutput
         } else {
         NSLog("Could not add movie file output to the session")
         self.setupResult = .sessionConfigurationFailed
         self.session.commitConfiguration()
         return
         }
         
         let stillImageOutput = AVCaptureStillImageOutput()
         if self.session.canAddOutput(stillImageOutput) {
         self.session.addOutput(stillImageOutput)
         self.stillImageOutput = stillImageOutput
         self.stillImageOutput!.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
         self.stillImageOutput!.isHighResolutionStillImageOutputEnabled = true
         } else {
         NSLog("Could not add still image output to the session")
         self.setupResult = .sessionConfigurationFailed
         self.session.commitConfiguration()
         return
         }*/
        
        self.backgroundRecordingID = UIBackgroundTaskInvalid
        
        chanfeFocus()
        
        // let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as AnyHashable as! String: Int(kCVPixelFormatType_32BGRA)]
        videoOutput.alwaysDiscardsLateVideoFrames = true
        
        let videoOutputQueue = DispatchQueue(label: "VideoQueue")
        videoOutput.setSampleBufferDelegate(self, queue: videoOutputQueue)
        if session.canAddOutput(videoOutput) {
            session.addOutput(videoOutput)
        } else {
            print("Could not add video data as output.")
        }
        
        self.session.commitConfiguration()
        self.session.startRunning()
    }
    
    
    //MARK:- AVCaptureVideoDataOutputSampleBufferDelegate
    func captureOutput(_ captureOutput: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!

        CVPixelBufferLockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))

        let width = CVPixelBufferGetWidth(imageBuffer)
        let height = CVPixelBufferGetHeight(imageBuffer)
        let bitsPerComponent = 8
        let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)

        let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer)!
        let byteBuffer = baseAddress.assumingMemoryBound(to: UInt8.self)

        for j in 0..<height {
            for i in 0..<width {
                let index = (j * width + i) * 4

                let b = byteBuffer[index]
                let g = byteBuffer[index+1]
                let r = byteBuffer[index+2]
                //let a = byteBuffer[index+3]

                if r > UInt8(128) && g < UInt8(128) {
                    byteBuffer[index] = UInt8(255)
                    byteBuffer[index+1] = UInt8(0)
                    byteBuffer[index+2] = UInt8(0)
                } else {
                    byteBuffer[index] = g
                    byteBuffer[index+1] = r
                    byteBuffer[index+2] = b
                }
            }
        }

        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGImageAlphaInfo.premultipliedFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue
        let newContext = CGContext(data: baseAddress, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo)
        
        if let context = newContext {
            let cameraFrame = context.makeImage()
            DispatchQueue.main.async {
               let image = UIImage(cgImage: cameraFrame!)
               let rgb = image.averageColor
                //print(rgb)
                
                //set first captured image
                self.testImg_one.image = image
                
                //get mat image
                self.testImg_two.image = OpenCVWrapper.toGray(image)
                
                let rgbMat = OpenCVWrapper.toGray(image).averageColor
                print(rgbMat)
            }
        }

        CVPixelBufferUnlockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
        
        
        let writable = canWrite()
        
        if writable,
            sessionAtSourceTime == nil {
            // start writing
            sessionAtSourceTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
            videoWriter?.startSession(atSourceTime: sessionAtSourceTime!)
            //print("Writing")
        }
        
        if captureOutput == videoOutput {
            connection.videoOrientation = .portrait
            
            if connection.isVideoMirroringSupported {
                connection.isVideoMirrored = true
            }
        }
        
        if writable,
            captureOutput == videoOutput,
            (videoWriterInput?.isReadyForMoreMediaData)! {
            // write video buffer
            videoWriterInput?.append(sampleBuffer)
            //print("video buffering")
        } else if writable,
            captureOutput == videoOutput,
            (audioWriterInput?.isReadyForMoreMediaData)! {
            // write audio buffer
            audioWriterInput?.append(sampleBuffer)
            //print("audio buffering")
        }
    }
    
    // MARK: Video Recording Methods
    func startRecording() {
        guard !isRecording else { return }
        isRecording = true
        setUpWriter()
        print(isRecording)
        print(videoWriter)
        if videoWriter?.status == .writing {
            print("status writing")
        } else if videoWriter?.status == .failed {
            print("status failed")
        } else if videoWriter?.status == .cancelled {
            print("status cancelled")
        } else if videoWriter?.status == .unknown {
            print("status unknown")
        } else {
            print("status completed")
        }
        
    }
    
    func stopRecording() {
        guard isRecording else { return }
        isRecording = false
        videoWriterInput?.markAsFinished()
        print("marked as finished")
        videoWriter?.finishWriting { [weak self] in
        }
        session.stopRunning()
        self.saveVideoToPhotoLibrary()
    }
    
    func saveVideoToPhotoLibrary(){
        
        print(self.outputFileLocation.path)
        let cleanup: ()->() = {
            if FileManager.default.fileExists(atPath: self.outputFileLocation.path) {
                do {
                    try FileManager.default.removeItem(at: self.outputFileLocation)
                } catch _ {}
            }
        }
        
            // Check authorization status.
            PHPhotoLibrary.requestAuthorization {status in
                guard status == .authorized else {
                    cleanup()
                    return
                }
                
                self.showLoader(strForMessage: "Saving the video...")
                // Save the movie file to the photo library and cleanup.
                PHPhotoLibrary.shared().performChanges({
                    
                    if #available(iOS 9.0, *) {
                        let options = PHAssetResourceCreationOptions()
                        options.shouldMoveFile = true
                        let changeRequest = PHAssetCreationRequest.forAsset()
                        changeRequest.addResource(with: .video, fileURL: self.outputFileLocation, options: options)
                    } else {
                        //### Error occurred while capturing movie
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: self.outputFileLocation)
                    }
                }, completionHandler: {success, error in
                    if !success {
                        NSLog("Could not save movie to photo library: \(error!)")
                    }
                    cleanup()
                    DispatchQueue.main.async {
                        self.dismissLoader()
                        self.navigationController?.popViewController(animated: false)
                    }
                   
                })
            }
         
    }
    
    //MARK:- Save video helpers
    func canWrite() -> Bool {
        return isRecording && videoWriter != nil && videoWriter?.status == .writing
    }
    
    //video file location method
    func videoFileLocation() -> URL {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let videoOutputUrl = URL(fileURLWithPath: documentsPath.appendingPathComponent("videoFile")).appendingPathExtension("mov")
        do {
            if FileManager.default.fileExists(atPath: videoOutputUrl.path) {
                try FileManager.default.removeItem(at: videoOutputUrl)
                print("file removed")
            }
        } catch {
            print(error)
        }
        
        return videoOutputUrl
    }

    
    func setUpWriter() {
        
        do {
            outputFileLocation = videoFileLocation()
            videoWriter = try AVAssetWriter(outputURL: outputFileLocation, fileType: AVFileType.mov)
            
            // add video input
            if #available(iOS 11.0, *) {
                videoWriterInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: [
                    AVVideoCodecKey : AVVideoCodecType.h264,
                    AVVideoWidthKey : 720,
                    AVVideoHeightKey : 1280,
                    AVVideoCompressionPropertiesKey : [
                        AVVideoAverageBitRateKey : 2300000,
                    ],
                    ])
            } else {
                videoWriterInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: [
                    AVVideoCodecKey : AVVideoCodecH264,
                    AVVideoWidthKey : 720,
                    AVVideoHeightKey : 1280,
                    AVVideoCompressionPropertiesKey : [
                        AVVideoAverageBitRateKey : 2300000,
                    ],
                    ])
            }
            
            videoWriterInput?.expectsMediaDataInRealTime = true
            
            if (videoWriter?.canAdd(videoWriterInput!))! {
                videoWriter?.add(videoWriterInput!)
                print("video input added")
            } else {
                print("no input added")
            }
            
            // add audio input
            audioWriterInput = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: nil)
            
            audioWriterInput?.expectsMediaDataInRealTime = true
            
            if (videoWriter?.canAdd(audioWriterInput!))! {
                videoWriter?.add(audioWriterInput!)
                print("audio input added")
            }
            
            
            videoWriter?.startWriting()
        } catch let error {
            debugPrint(error.localizedDescription)
        }
        
        
    }

    @objc func updateTimer() {
      // self.performSegue(withIdentifier: "id_web", sender: self)
    }
    
    //MARK: ### Compatibility
    @available(iOS, deprecated: 10.0)
    private class func deviceWithMediaType(_ mediaType: String, preferringPosition position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let devices = AVCaptureDevice.devices(for: AVMediaType(rawValue: mediaType))
        var captureDevice = devices.first
        
        for device in devices {
            if device.position == position {
                captureDevice = device
                break
            }
        }
        
        return captureDevice
    }
        @available(iOS, deprecated: 10.0)
    
    
    private func snapStillImage() {
       
            let stillImageConnection = self.stillImageOutput!.connection(with: AVMediaType.video)
            let previewLayer = self.previewView.layer as! AVCaptureVideoPreviewLayer
            
            // Update the orientation on the still image output video connection before capturing.
            stillImageConnection?.videoOrientation = (previewLayer.connection?.videoOrientation)!
            
            // Flash set to Auto for Still Capture
            
            let lensStabilizationEnabled: Bool
            if #available(iOS 9.0, *) {
                lensStabilizationEnabled = self.stillImageOutput!.isLensStabilizationDuringBracketedCaptureEnabled
            } else {
                lensStabilizationEnabled = false
            }
            if !lensStabilizationEnabled {
                // Capture a still image
                self.stillImageOutput?.captureStillImageAsynchronously(from: self.stillImageOutput!.connection(with: AVMediaType.video)!) {imageDataSampleBuffer, error in
                    
                    if error != nil {
                        NSLog("Error capture still image \(error!)")
                    } else if imageDataSampleBuffer != nil {
                        let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer!)!
                        
                        PHPhotoLibrary.requestAuthorization {status in
                            if status == PHAuthorizationStatus.authorized {
                                if #available(iOS 9.0, *) {
                                    PHPhotoLibrary.shared().performChanges({
                                        PHAssetCreationRequest.forAsset().addResource(with: PHAssetResourceType.photo, data: imageData, options: nil)
                                    }, completionHandler: {success, error in
                                        if !success {
                                            NSLog("Error occured while saving image to photo library: \(error!)")
                                        }
                                    })
                                } else {
                                    let temporaryFileURL: URL
                                    if #available(iOS 10.0, *) {
                                        temporaryFileURL = FileManager.default
                                            .temporaryDirectory
                                            .appendingPathComponent(ProcessInfo().globallyUniqueString)
                                            .appendingPathExtension("jpg")
                                    } else {
                                        let temporaryFileName = ProcessInfo().globallyUniqueString as NSString
                                        let temporaryFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(temporaryFileName.appendingPathExtension("jpg")!)
                                        temporaryFileURL = URL(fileURLWithPath: temporaryFilePath)
                                    }
                                    
                                    PHPhotoLibrary.shared().performChanges({
                                        do {
                                            try imageData.write(to: temporaryFileURL, options: .atomicWrite)
                                            PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: temporaryFileURL)
                                        } catch let error {
                                            NSLog("Error occured while writing image data to a temporary file: \(error)")
                                        }
                                    }, completionHandler: {success, error in
                                        if !success {
                                            NSLog("Error occurred while saving image to photo library: \(error!)")
                                        }
                                        
                                        // Delete the temporary file.
                                        do {
                                            try FileManager.default.removeItem(at: temporaryFileURL)
                                        } catch _ {}
                                    })
                                }
                            }
                        }
                    }
                }
            } else {
                if #available(iOS 9.0, *) {
                    // Capture a bracket
                    let bracketSettings: [AVCaptureBracketedStillImageSettings]
                    if self.videoDevice!.exposureMode == .custom {
                        bracketSettings = [AVCaptureManualExposureBracketedStillImageSettings.manualExposureSettings(exposureDuration: AVCaptureDevice.currentExposureDuration, iso: AVCaptureDevice.currentISO)]
                    } else {
                        bracketSettings = [AVCaptureAutoExposureBracketedStillImageSettings.autoExposureSettings(exposureTargetBias: AVCaptureDevice.currentExposureTargetBias)];
                    }
                    
                    self.stillImageOutput!.captureStillImageBracketAsynchronously(from: self.stillImageOutput!.connection(with: AVMediaType.video)!,
                                                                                  withSettingsArray: bracketSettings
                        
                    ) {imageDataSampleBuffer, stillImageSettings, error in
                        if error != nil {
                            NSLog("Error bracketing capture still image \(error!)")
                        } else if imageDataSampleBuffer != nil {
                            NSLog("Lens Stabilization State: \(CMGetAttachment(imageDataSampleBuffer!, kCMSampleBufferAttachmentKey_StillImageLensStabilizationInfo, nil)!)")
                            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer!)
                            
                            PHPhotoLibrary.requestAuthorization {status in
                                if status == PHAuthorizationStatus.authorized {
                                    PHPhotoLibrary.shared().performChanges({
                                        PHAssetCreationRequest.forAsset().addResource(with: PHAssetResourceType.photo, data: imageData!, options: nil)
                                    }, completionHandler: {success, error in
                                        if !success {
                                            NSLog("Error occured while saving image to photo library: \(error!)")
                                        }
                                    })
                                }
                            }
                        }
                    }
                }
        }
    }
    
}


extension UIImage {
    var averageColor: UIColor? {
        guard let inputImage = CIImage(image: self) else { return nil }
        let extentVector = CIVector(x: inputImage.extent.origin.x, y: inputImage.extent.origin.y, z: inputImage.extent.size.width, w: inputImage.extent.size.height)
        
        guard let filter = CIFilter(name: "CIAreaAverage", withInputParameters: [kCIInputImageKey: inputImage, kCIInputExtentKey: extentVector]) else { return nil }
        guard let outputImage = filter.outputImage else { return nil }
        
        var bitmap = [UInt8](repeating: 0, count: 4)
        let context = CIContext(options: [kCIContextWorkingColorSpace: kCFNull])
        context.render(outputImage, toBitmap: &bitmap, rowBytes: 4, bounds: CGRect(x: 0, y: 0, width: 1, height: 1), format: kCIFormatRGBA8, colorSpace: nil)
        
        return UIColor(red: CGFloat(bitmap[0]) / 255, green: CGFloat(bitmap[1]) / 255, blue: CGFloat(bitmap[2]) / 255, alpha: CGFloat(bitmap[3]) / 255)
    }
}

