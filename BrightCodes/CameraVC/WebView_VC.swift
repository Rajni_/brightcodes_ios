//
//  WebView_VC.swift
//  BrightCodes
//
//  Created by Krescent Global on 23/08/18.
//  Copyright © 2018 ksglobal. All rights reserved.
//

import UIKit
import WebKit

class WebView_VC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var webView: UIWebView!
    
    
    //MARK:- UIView life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //hide title
        self.navigationController?.navigationBar.backItem?.title = ""
        
        //load url
        let url = URL(string: "http://mybright.codes/home/1023?uid=1021")
        let request = URLRequest(url: url!)
        webView.loadRequest(request)
    }
    
    //MARK:- UIButton Actions
    @IBAction func barBtn_back_action(_ sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: false)
    }
    


}
