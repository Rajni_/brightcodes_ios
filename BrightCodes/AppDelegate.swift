//
//  AppDelegate.swift
//  BrightCodes
//
//  Created by ksglobal on 06/08/18.
//  Copyright © 2018 ksglobal. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        logUser()
        return true
    }
    
    func logUser() {
        Crashlytics.sharedInstance().setUserEmail("user@fabric.io")
        Crashlytics.sharedInstance().setUserIdentifier("12345")
        Crashlytics.sharedInstance().setUserName("Test User")
    }

}

