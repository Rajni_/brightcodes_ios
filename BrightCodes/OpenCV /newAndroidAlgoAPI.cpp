

#include <stdio.h>
#include <iostream>
#include <cstdio>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include <numeric>
#include <vector>
#include "timer.h"
#include <algorithm>

#include "brightcodes.h"
#include "newAndroidAlgoAPI.h"

//OpenCV

#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <regex>


namespace brightcodes {


	StateData stateData;
	//bool fromVideoFile = false;
	//std::string algoPath="/sdcard/DCIM/Algo/";

	inline void rotate_image_90(Mat & img, const bool clockwise = true)
	{
		transpose(img, img);
		if (clockwise)
			flip(img, img, 1);  // rotate clockwise
		else
			flip(img, img, 0);  // rotate clockwise
		return;
	}

	StatusData  extractCodeFromFrames(const cv::Mat& mat, int nExpLeds, float aoiSz, string optCols, int resetState, int temporalMaskFramesNum, vector<string>& leds_codes, Mat& temporalMask)
	{
		static vector<cv::Mat> framesArr(temporalMaskFramesNum);
		static int frameCounter = 0;

		StatusData resultStatus;
		if (resetState)
		{
			framesArr.clear();
			frameCounter = 0;
			stateData.reset();
		}

		Mat croppedFrame;
		//changeFrameSize(mat, croppedFrame, aoiSz);

		try {
			
			if (frameCounter < temporalMaskFramesNum)
			{
				framesArr.push_back(croppedFrame);
			}
			frameCounter++;
			if (frameCounter == temporalMaskFramesNum)
			{
				Mat mask = detectLedsTemporal(framesArr);
				temporalMask = mask.clone();
				stateData.hThr = calculateHueBasedThresholdDynamicAll(framesArr, mask);
				//                __android_log_print(ANDROID_LOG_DEBUG, "native",
				//                                    "calculateHueBasedThresholdDynamicAll - %f", timer.Elapsed().count());
							// Calculate the hue thresholds


							// direction of division
				stateData.portrait = framesArr[0].size().height > framesArr[0].size().width;

				
				if (!processTemporalMask(stateData.portrait, stateData.rot, stateData.fullFrameSlope, stateData.fullFrameOffset, stateData.estAngle, mask))
				{
					resultStatus.status = StatusData::FATAL;
					resultStatus.errorMsg = "Failed to process temporal mask - probably empty";
					return resultStatus;
				}


				// extract leds code from the first frame
				int idx_frame_detect = 0;
				stateData.success_leds_detect = false;
				while (!stateData.success_leds_detect && idx_frame_detect < temporalMaskFramesNum)
				{
					string code;
					for (int i = 0; i < nExpLeds; i++)
						code += "u";
					try
					{
						//code = detectLedsNoAoiNoTemplate(framesArr, nExpLeds, optCols, idx_frame_detect, portrait, begPoint, endPoint, estPitch, rot, hThr, mask);
						code = detectLedsNoAoiNoTemplate(framesArr, nExpLeds, optCols, idx_frame_detect, stateData, mask);
					}
					catch (AlgorithmError& ex)
					{
						resultStatus.errorMsg = resultStatus.errorMsg + ex.what()+'\n';
						resultStatus.status = StatusData::WARNING;
					}
					catch (exception &ex)
					{
						resultStatus.status = StatusData::FATAL;
						resultStatus.errorMsg = "Exception thrown by detection of leds without template and aoi\nException: " \
							+ string(ex.what());
						return resultStatus;
					}
					int cnt = 0;
					for (auto s : code)
						cnt += int(s == 'u');
					if (cnt <= nExpLeds * 2 / 5)
						stateData.success_leds_detect = true;

					leds_codes.push_back(code);
					idx_frame_detect++;
				}
				// throw exception!!!
				if (!stateData.success_leds_detect)
				{
					resultStatus.status = StatusData::FATAL;
					resultStatus.errorMsg = "extract frame code from first frame failed";
					return resultStatus;
				}

				// extract leds code from the next frames that have already been read
				for (; idx_frame_detect < framesArr.size(); idx_frame_detect++)
				{
					//Omri - this was commented - but if frames already read - we didnt rotate them until now
					if (stateData.rot)
						rotate_image_90(framesArr[idx_frame_detect]);

					string code;
					for (int i = 0; i < nExpLeds; i++)
					{
						code += "u";
					}
					try
					{
						auto currFrameToWatch = framesArr[idx_frame_detect];
						code = detedLedsInAoi(framesArr[idx_frame_detect], nExpLeds, optCols, stateData.portrait, stateData.begPoint, stateData.endPoint, stateData.estPitch, stateData.hThr);
					}
					catch (AlgorithmError& ex)
					{
						resultStatus.errorMsg = resultStatus.errorMsg + ex.what() + '\n';
						resultStatus.status = StatusData::WARNING;
					}
					catch (exception &ex)
					{
						/*__android_log_print(ANDROID_LOG_DEBUG, "native",
						"detectLedsNoAoiNoTemplate error: %s",
						ex.err_msg.c_str());*/
			
                        /*for (int i = 0; i < nExpLeds; i++)
						code += "u";*/
						resultStatus.status = StatusData::FATAL;
						resultStatus.errorMsg = "Exception thrown by detection of leds in aoi\nException: " \
							+ string(ex.what());
						continue;
					}
					leds_codes.push_back(code);
				}
			}

			else if (frameCounter > temporalMaskFramesNum)
			{
				// extract leds code from the rest of the frames
				//__android_log_print(ANDROID_LOG_DEBUG, "native", "extract from video");
				int cnt = 0;
				int total_frame_number = 1;//FramesStream.get(CV_CAP_PROP_FRAME_COUNT);

				if (stateData.rot)
					rotate_image_90(croppedFrame);

				string code;
				for (int i = 0; i < nExpLeds; i++)
					code += "u";
				try
				{
					code = detedLedsInAoi(croppedFrame, nExpLeds, optCols, stateData.portrait, stateData.begPoint, stateData.endPoint, stateData.estPitch, stateData.hThr);
				}
				catch (AlgorithmError& ex)
				{
					resultStatus.errorMsg = ex.what();
					resultStatus.status = StatusData::WARNING;
				}
				catch (exception &ex)
				{
					/*__android_log_print(ANDROID_LOG_DEBUG, "native",
					"detectLedsNoAoiNoTemplate error: %s",
					ex.err_msg.c_str());*/
					/*for (int i = 0; i < nExpLeds; i++)
					code += "u";*/
					resultStatus.status = StatusData::FATAL;
					resultStatus.errorMsg = "Exception thrown by detection of leds in aoi\nException: " \
						+ string(ex.what());
					return resultStatus;
				}
				leds_codes.push_back(code);

			}

			return resultStatus;
		}
		catch (AlgorithmError& ex)
		{
			resultStatus.errorMsg = ex.what();
			resultStatus.status = StatusData::WARNING;
		}
		catch (exception &ex)
		{
			/*__android_log_print(ANDROID_LOG_DEBUG, "native",
			"detectLedsNoAoiNoTemplate error: %s",
			ex.err_msg.c_str());*/
			/*for (int i = 0; i < nExpLeds; i++)
			code += "u";*/
			resultStatus.status = StatusData::FATAL;
			resultStatus.errorMsg = "Exception thrown by detection of leds in aoi\nException: " \
				+ string(ex.what());
			return resultStatus;
		}
		/*catch (...) {
			if (std::exception_ptr pErr = std::current_exception()) {
				throwJavaException(env, pErr.__cxa_exception_type()->name());
			}
			else
				throwJavaException(env, "unknown error");
		}*/
		
		return resultStatus;
	}
}


//********************************************************************//
//
//JNIEXPORT void JNICALL Java_com_example_ndk_1opencv_1androidstudio_NativeClass_nativeAddMat
//(JNIEnv * env, jclass clazz, jlong ptr, jobject jCodes, jint nExpLeds, jfloat aoiSz, jstring jOptCols) {
//	__android_log_print(ANDROID_LOG_DEBUG, "native", "nativeAddMat: %d", frame_counter);
//
//	initMatDebug(env);
//
//	if (!stream) {
//		stream = make_shared<FramesStreamConnAndroid>(aoiSz);
//	}
//
//	bool readNextFromFile = true;
//	if (fileStream == nullptr && fromVideoFile && frame_counter == 0)
//	{
//		fileStream = make_shared<VideoCapture>();
//		string s = algoPath + "debug_frames/" + "%03d.jpg";
//		//000_1mat.jpg
//
//		fileStream->open(s);
//		if (!fileStream->isOpened()) {
//			throw AlgorithmError("Failed to open movie file");
//		}
//	}
//
//	jboolean isCopy;
//	string optCols = env->GetStringUTFChars(jOptCols, &isCopy);
//
//	//Timer timerDebug(true);
//	//Java_com_example_ndk_1opencv_1androidstudio_NativeClass_rotateAndDebug(env, clazz, ptr);
//	//__android_log_print(ANDROID_LOG_DEBUG, "native", "otateAndDebug - %f", timerDebug.Elapsed().count());
//
//	Timer timerFrameRead(true);
//
//	cv::Mat& mat = *(cv::Mat*)ptr;
//
//	//frames.push_back(mat);
//	*stream << mat;
//	frame_counter++;
//
//	//__android_log_print(ANDROID_LOG_DEBUG, "native", "read to stream - %f", timerFrameRead.Elapsed().count());
//
//	vector<string> leds_codes;
//	// This method is replacement of original extractBrightCodeFromVideo with ability
//	// to process images as they come
//	const int temporalMaskFramesNum = 5;
//	const int maskThr = 10;
//
//	try {
//		if (frame_counter < temporalMaskFramesNum) {
//			return;
//		}
//		else if (frame_counter == temporalMaskFramesNum) {
//			// read the first frames for temporal mask creation
//			vector<Mat> framesArr;
//			if (!fromVideoFile) {
//				Timer timer;
//
//				readFirstFrames(*stream, temporalMaskFramesNum, aoiSz, framesArr);
//				//std::cout << "readFirstFrames: " << std::fixed << timer << "ms\n";
//				//__android_log_print(ANDROID_LOG_DEBUG, "native", "readFirstFrames - %f", timer.Elapsed().count());
//
//			}
//			else
//			{
//				readFirstFrames(*fileStream, temporalMaskFramesNum, aoiSz, framesArr);
//			}
//
//
//			//            Timer timerTemporal(true);
//
//
//			Mat mask = detectLedsTemporal(framesArr);
//			//            __android_log_print(ANDROID_LOG_DEBUG, "native",
//			//                                "detectLedsTemporal - %f", timerTemporal.Elapsed());
//			//            timerTemporal.Reset();
//			//            Mat mask2 = detectLedsTemporal2(framesArr);
//			//            __android_log_print(ANDROID_LOG_DEBUG, "native",
//			//                                "detectLedsTemporal2 - %f", timerTemporal.Elapsed());
//
//						//imwrite( algoPath+"temporalMask.jpg", mask );
//
//			{
//				Timer timer(true);
//				hThr = calculateHueBasedThresholdDynamicAll(framesArr, mask);
//				//                __android_log_print(ANDROID_LOG_DEBUG, "native",
//				//                                    "calculateHueBasedThresholdDynamicAll - %f", timer.Elapsed().count());
//			}
//			// Calculate the hue thresholds
//			//hThr = calculateHueBasedThreshold(framesArr,mask);
//			std::stringstream ss;
//			for (auto t : hThr)
//				ss << t << ", ";
//			__android_log_print(ANDROID_LOG_DEBUG, "native", "hthr = %s", ss.str().c_str());
//			// direction of division
//			portrait = framesArr[0].size().height > framesArr[0].size().width;
//			//float estPitch;
//			//bool rot;
//			//Point begPoint, endPoint;
//
//			if (nExpLeds == 1) {
//				// Create the temporal mask
//				Mat temporalMask = detectLedTemporal(framesArr);
//
//				// Detect leds by temporal changes
//				if (debug & false)
//				{
//					imshow("detectLedsNoAoiNoTemplate: mask", temporalMask);
//					waitKey();
//				}
//
//				// Throw an error if less then a certain number of pixels are LED pixels
//				if (sum(temporalMask > 0)[0] < maskThr)
//					throw AlgorithmError("sum(temporalMask > 0)[0] < maskThr");
//
//				// Clean mask
//				Mat clMask = cleanMask(temporalMask);
//				if (debug & false)
//				{
//					imshow("detectLedsNoAoiNoTemplate: clmask", clMask);
//					waitKey();
//				}
//
//				// extract led codes
//				int total_frame_number = framesArr.size() + stream->length();
//				for (int i = 0; i < total_frame_number; i++)
//				{
//					Mat frame;
//					if (i < framesArr.size())
//						frame = framesArr[i];
//					else
//						*stream >> frame;
//
//					if (debug & false)
//						cout << i << " " << (i) * 100 / total_frame_number << "%" << endl;
//
//					string code;
//					try
//					{
//						code = detectLed(clMask, frame, optCols, portrait, hThr);
//					}
//					catch (AlgorithmError)
//					{
//						code = "u";
//					}
//					leds_codes.push_back(code);
//				}
//
//				yieldCodes(env, jCodes, leds_codes);
//
//				return;
//			}
//
//			Timer timer(true);
//
//			// extract leds code from the first frame
//			int idx_frame_detect = 0;
//			success_leds_detect = false;
//			while (!success_leds_detect && idx_frame_detect < temporalMaskFramesNum)
//			{
//				string code;
//				try
//				{
//					code = detectLedsNoAoiNoTemplate(framesArr, nExpLeds, optCols, idx_frame_detect, portrait, begPoint, endPoint, estPitch, rot, hThr, mask);
//				}
//				catch (AlgorithmError &ex)
//				{
//					__android_log_print(ANDROID_LOG_DEBUG, "native",
//						"detectLedsNoAoiNoTemplate error: %s",
//						ex.err_msg.c_str());
//					for (int i = 0; i < nExpLeds; i++)
//						code += "u";
//				}
//				int cnt = 0;
//				for (auto s : code)
//					cnt += int(s == 'u');
//				if (cnt <= nExpLeds * 2 / 5)
//					success_leds_detect = true;
//				__android_log_print(ANDROID_LOG_DEBUG, "native",
//					"first frames. %d, s='%s', success_leds_detect=%d",
//					idx_frame_detect,
//					code.c_str(),
//					success_leds_detect);
//				leds_codes.push_back(code);
//				idx_frame_detect++;
//			}
//			// throw exception!!!
//			if (!success_leds_detect)
//				throw AlgorithmError("extract frame code from first frame failed");
//
//			//            __android_log_print(ANDROID_LOG_DEBUG, "native",
//			//                                "detectLedsNoAoiNoTemplate - %f", timer.Elapsed().count());
//			timer.Reset();
//			// extract leds code from the next frames that have already been read
//			for (; idx_frame_detect < framesArr.size(); idx_frame_detect++)
//			{
//				//Omri - this was commented - but if frames already read - we didnt rotate them until now
//				if (rot)
//					stream->rotate_image_90(framesArr[idx_frame_detect]);
//				string code = detedLedsInAoi(framesArr[idx_frame_detect], nExpLeds, optCols, portrait, begPoint, endPoint, estPitch, hThr);
//				leds_codes.push_back(code);
//			}
//
//			//            __android_log_print(ANDROID_LOG_DEBUG, "native",
//			//                                "detedLedsInAoi first frames - %f", timer.Elapsed().count());
//						//Omri
//			readNextFromFile = false;
//		}
//
//		//Omri - set rotate in stream..
//		if (rot)
//			stream->is2rot = rot;
//
//		// extract leds code from the rest of the frames
//		//__android_log_print(ANDROID_LOG_DEBUG, "native", "extract from video");
//		int cnt = 0;
//		int total_frame_number = 1;//FramesStream.get(CV_CAP_PROP_FRAME_COUNT);
//
//
//		if (debug & false)
//			cout << ++cnt << " " << (idx_frame_detect + cnt) * 100 / total_frame_number << "%" << endl;
//		Mat frame;
//		if (fromVideoFile)
//		{
//			if (readNextFromFile) {
//				*fileStream >> frame;
//			}
//			readNextFromFile = !readNextFromFile;
//		}
//		else {
//			*stream >> frame;
//		}
//		if (frame.empty())
//			break;
//		// We do all rotations and size changes inside of stream
//		//frame = changeFrameSize(frame, aoiSz);
//		//if (rot)
//		//	rotate_image_90(frame);
//		//imshow("Non first frame rotated and pre-cropped to ROI", frame);
//		//waitKey(10);
//		string code = detedLedsInAoi(frame, nExpLeds, optCols, portrait, begPoint, endPoint, estPitch, hThr);
//		leds_codes.push_back(code);
//
//
//		return resultStatus;
//
//	}
//	catch (const AlgorithmError& err) {
//		//throw AlgorithmError();
//		__android_log_print(ANDROID_LOG_DEBUG, "native",
//			"detedLedsInAoi error: %s",
//			err.err_msg.c_str());
//		throwJavaException(env, "Failed to detect code");
//	}
//	catch (const std::exception& err) {
//		// OpenCV go here
//		//throwJavaException(env, err.what());
//		__android_log_print(ANDROID_LOG_DEBUG, "native",
//			"OpenCV error: %s",
//			err.what());
//		throwJavaException(env, "Failed to detect code");
//	}
//	catch (...) {
//		if (std::exception_ptr pErr = std::current_exception()) {
//			throwJavaException(env, pErr.__cxa_exception_type()->name());
//		}
//		else
//			throwJavaException(env, "unknown error");
//	}
