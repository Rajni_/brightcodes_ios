//
// Created by VE on 30/10/2017.
//

#ifndef BRIGHTCODES_ANDROID_ML_DECODING_H
#define BRIGHTCODES_ANDROID_ML_DECODING_H

#include <string>
#include <vector>
int decodeRows(int *vec, int N, bool useTwoCols, int *cands, int &cands_num, int cands_num_max);

int* convertCodeStrToIntForDecoding(std::vector<std::string>& codes);

#endif //BRIGHTCODES_ANDROID_ML_DECODING_H
