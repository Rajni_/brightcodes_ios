#ifndef __BRIGHTCODE_H__
#define __BRIGHTCODE_H__

#ifndef DEBUG
//#define DEBUG
#endif

#ifndef __linux__
//#include "stdafx.h"
#endif
#include <stdio.h>
#include <iostream>
#include <cstdio>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <sstream>

//OpenCV
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

#define EPSILON 0.000001

namespace brightcodes
{
	extern const int temporalMaskFramesNum;
	extern const int temporalMaskFramesNum2;
	extern const bool saveResultsToFile;
        extern const int maxFirstFrameRead;
        extern const int NumConsecutiveRequired;
        extern const int numMinimumRead;
	extern const int maskThr;
	extern const double redIncThr;
	extern const double ratioThr;
	extern const double prcThr;
	extern const int minArea;
	extern vector<float> prevCenters;

	class StateData
	{
	public:
		void reset()
		{
			estPitch = 0.0;
			rot = false;
			begPoint = Point();  endPoint = Point();
			idx_frame_detect = 0;
			hThr.clear();
			//vector<Mat> frames;
			success_leds_detect = false;
			cnt = 0;
			portrait = false;

			slope = 0.0;
			offset = 0.0;
			estAngle = 0.0;
			fullFrameSlope = 0.0;
			fullFrameOffset = 0.0;
		}
	public:
		float estPitch = 0.0;
		bool rot = false;
		cv::Point begPoint, endPoint;
		int idx_frame_detect;
		std::vector<int> hThr;
		//vector<Mat> frames;
		bool success_leds_detect = false;
		int cnt = 0;
		bool portrait = false;

		float slope = 0.0, offset = 0.0, estAngle = 0.0;
		float fullFrameSlope = 0.0, fullFrameOffset = 0.0;
	};


	// exceptions
	class AlgorithmError:  public exception
	{
		public:
			std::string info;
			AlgorithmError (String _info)
			{
				info = _info;
			}
			virtual const char* what() const throw()
			{
				return info.c_str();
			}
	};
	class NoLedsFound : public AlgorithmError {};
	class PeaksNotFound : public AlgorithmError {};

	Mat changeFrameSize(const Mat& frame, float aoiSz);
	void readFirstFrames(VideoCapture& fs, const int n_frames, const float aoiSz, vector<Mat>& frame_array);
	Mat createBMask(const Mat &frame);
	Mat createBMask(const vector<Mat> &bgr32SC1);
	Mat createRMask(const Mat &frame);
	Mat createRMask(const vector<Mat> &bgr32SC1);
	Mat ceateRPMask(const Mat&frame);
	Mat ceateRPMask(const vector<Mat> &bgr32SC1);

	Mat detectLedsTemporal(vector<Mat> &framesArr);
	Mat cleanMask(const Mat& mask);
	Point locateLineAOI(const Mat& vec);
	void cropMask(const Mat& mask, Mat& cMask, Point& beRow, Point& beCol);
	void detectLedsLineByRadon(const Mat& mask, const Point& imCenterTrans, float& slope, float& offset, float& estAngle);
	vector<float> cutLineVecFromIm(const Mat& im, const float slope, const float offset);
	vector<float> extract_rb_Line(const Mat& im, const float slope, const float offset);
	double detectPitch(const Mat& X);
	void estimatePitchByFFT(const Mat& im, const Mat& mask, const float slope, const float offset, float &pitch, vector<Point>& cutXY);
	void cutMaskByPitch(const Mat& mask, const double estPitch, const vector<Point>& cutXY, Mat& cutMask, Point& begPoint, Point& endPoint);
	template<typename T> vector<T> vecDiff(const vector<T>& vec);
	vector<float> findLocalMax1D(const vector<float>& vec);
	vector<float> locatePeaks(vector<float>& vec, double estPitch, int nExpLeds);
	void getLedPixels(const Point& center, const Size& imSz, const float radius, const bool portait, vector<Point>& upperPix, vector<Point>& lowerPix);
	string getLedColor(const Mat& img, vector<Point> led_mask, string optCols, vector<int> hThr);
	bool refineLineParams(const Mat& mask, const int nExpLeds, float &slope, float &offset);
	void calcBegAndEnd(const Point& maskBedPoint, const vector<Point>& centersLoc, const float pitch, const Size& imSz, Point& begPoint, Point& endPoint);
	string detedLedsInAoi(const Mat& frame, const int nExpLeds, const string optCols, const bool portrait, Point& begPoint, Point& endPoint, float& pitch, vector<int> hThr);

	vector<string> extractBrightCodeFromVideo(string vidPath, int nExpLeds = 27, float aoiSz = 1.0, string optCols = "rb");
	string detectLedsNoAoiNoTemplate(vector<Mat>& framesArr, int nExpLeds, string optCols, int frame_idx, bool portrait, Point& begPoint, Point& endPoint, float& estPitch, bool& rot, vector<int> hThr,Mat& mask);
	string detectLedsNoAoiNoTemplate(vector<Mat>& framesArr, int nExpLeds, string optCols, int frame_idx, StateData& stateData, Mat& mask);

	vector<int> c(vector<Mat> framesArr);
	string getLedSetCode(vector<Point> centers, int nExpLeds, string optCols, const Mat& im, bool portait, vector<int> hThr);
	//vector<Point> estimateMissingCenters(vector<Point> centers, int nExpLeds, const float estPitch, vector<Point> prevCenters);
	vector<float> estimateMissingCenters(vector<float> centers, int nExpLeds, const float estPitch, vector<Point> prevCenters);
	
	void extractXYCoords(const Mat& im, const float slope, const float offset, vector<int>& xv, vector<int>& yv);

	vector<int> calculateHueBasedThresholdDynamicAll(const vector<Mat>& framesArr, Mat &mask);
	bool processTemporalMask(bool& portrait, bool& rot, float& slope, float& offset, float& estAngle, Mat &mask);


}

#endif // __BRIGHTCODE_H__
