//
//  OpenCVWrapper.h
//  BrightCodes
//
//  Created by Krescent Global on 27/08/18.
//  Copyright © 2018 ksglobal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject

- (void)isThisWorking;

+ (UIImage *)toGray:(UIImage *)source;

//- (void)extractCodeFromFrames:(const*)cv::Mat& mat (int*)nExpLeds (float*)aoiSz (String*)optCols (int*)resetState (int*)temporalMaskFramesNum
//
//:(NSString*)busStop
//
//StatusData  extractCodeFromFrames(const cv::Mat& mat, int nExpLeds, float aoiSz, string optCols, int resetState, int temporalMaskFramesNum, vector<string>& leds_codes, Mat& temporalMask)

@end
