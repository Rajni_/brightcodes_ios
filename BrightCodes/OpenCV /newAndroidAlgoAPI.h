#pragma once
#include "opencv2/opencv.hpp"
#include "opencv2/core/types.hpp"
#include <string>
#include <vector>

namespace brightcodes {
    
    
    
	typedef struct StatusData
	{
		int status = OK;
		std::string errorMsg = "";
		enum StatusType { OK = 0, WARNING, FATAL };
	} StatusData;



    StatusData  extractCodeFromFrames(const cv::Mat& mat, int nExpLeds, float aoiSz, std::string optCols, int resetState, int temporalMaskFramesNum, std::vector<std::string>& leds_codes, cv::Mat& temporalMask);

	inline void changeFrameSize(const cv::Mat& frame, cv::Mat& rs_frame, float ROI_Size)
	{
		if (ROI_Size > 0.0 && ROI_Size < 1.0)
		{
			double axAoiSz = sqrt(ROI_Size);
			int cutSz_cols = (int)round((1.0 - axAoiSz) / 2.0 * float(frame.cols));
			int cutSz_rows = (int)round((1.0 - axAoiSz) / 2.0 * float(frame.rows));
            
			Rect rect(cutSz_cols, cutSz_rows, frame.cols - 2 * cutSz_cols, frame.rows - 2 * cutSz_rows);
			rs_frame = frame(rect);
		}
		else
			rs_frame = frame;
		return;
	}
    
    /*In newAndroidAlgoAPI.h file we have a function named "changeFrameSize".
     Inside this function we are using "Rect" object but we have not provided this an intialise value. Now when i try to call "extractCodeFromFrames" it gives me an error of "No matching constructor for initialization of 'Rect'".
     
     
     */
    
}
