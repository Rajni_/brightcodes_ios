#ifndef __linux__
//#include "stdafx.h"
#endif
#include <stdio.h>
#include <iostream>
#include <cstdio>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include <numeric>
#include <vector>
#include "timer.h"
#include <algorithm>

#include "brightcodes.h"

#define DEBUG
bool debug = false;
//#define #saveResultsToFile

 //#define PROFILE

#define convertHoughToMatlabRadon(x) ((180 - x) % 180)

 //OpenCV
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <regex>


using namespace cv;
using namespace std;


namespace brightcodes
{
	const int temporalMaskFramesNum = 5;
	const int temporalMaskFramesNum2 = 10;
	const int maxFirstFrameRead = 14;
	const int NumConsecutiveRequired = 7;
	const int numMinimumRead = 7;
	const int maskThr = 10;
	const double redIncThr = 25; //50; // 50
	const double ratioThr = 2.5; // 3.5
	const double prcThr = 0.75;
	const int minArea = 5;
	
	const float DEG2RAD = CV_PI / 180;
	const float RAD2DEG = 180 / CV_PI;

	std::vector<float> brightcodes::prevCenters;

	template <typename T>
	cv::Mat plotGraph(std::vector<T>& vals, int YRange[2], CvScalar color = Scalar(255, 0, 0))
	{
		auto it = minmax_element(vals.begin(), vals.end());
		float scale = 1. / ceil(*it.second - *it.first);
		float bias = *it.first;
		int rows = YRange[1] - YRange[0] + 1;
		cv::Mat image = Mat::zeros(rows, vals.size(), CV_8UC3);
		image.setTo(0);
		for (int i = 0; i < (int)vals.size() - 1; i++)
			cv::line(image, cv::Point(i, rows - 1 - (vals[i] - bias)*scale*YRange[1]), cv::Point(i + 1, rows - 1 - (vals[i + 1] - bias)*scale*YRange[1]), color , 1);
		return image;
	}

	string type2str(int type)
	{
		string r;

		uchar depth = type & CV_MAT_DEPTH_MASK;
		uchar chans = 1 + (type >> CV_CN_SHIFT);

		switch (depth)
		{
		case CV_8U:  r = "8U"; break;
		case CV_8S:  r = "8S"; break;
		case CV_16U: r = "16U"; break;
		case CV_16S: r = "16S"; break;
		case CV_32S: r = "32S"; break;
		case CV_32F: r = "32F"; break;
		case CV_64F: r = "64F"; break;
		default:     r = "User"; break;
		}

		r += "C";
		r += (chans + '0');

		return r;
	}
	inline void rotate_image_90(Mat & img)
	{
		transpose(img, img);
		flip(img, img, 1);  // rotate clockwise
		return;
	}
	template <typename T>
	T median(vector<T> vec)
	{
		size_t n = vec.size();
		partial_sort(vec.begin(), vec.begin() + n / 2 + 1, vec.end());
		T med;
		if (n % 2 == 0)
			med = 0.5 *(vec[n / 2] + vec[n / 2 - 1]);
		else
			med = vec[(n - 1) / 2];
		return med;
	}

	Mat changeFrameSize(const Mat& frame, float aoiSz)
	{
		Mat rs_frame;
		if (aoiSz > 0.0 && aoiSz < 1.0)
		{
			double axAoiSz = sqrt(aoiSz);
			int cutSz_cols = (int)round((1.0 - axAoiSz) / 2.0 * float(frame.cols));
			int cutSz_rows = (int)round((1.0 - axAoiSz) / 2.0 * float(frame.rows));
			Rect rect(cutSz_cols, cutSz_rows, frame.cols - 2 * cutSz_cols, frame.rows - 2 * cutSz_rows);
			rs_frame = frame(rect);
		}
		else
			rs_frame = frame;
		return rs_frame;
	}

	void splitFrame2Channels32SC1(Mat& frame, vector<Mat>& channels, bool b, bool g, bool r)
	{
		split(frame, channels);
		//matlab: b = pic(:,:,2) / 3;
		if (b)
		channels[0].convertTo(channels[0], CV_32SC1);
		if (g)
		channels[1].convertTo(channels[1], CV_32SC1);
		if (r)
		channels[2].convertTo(channels[2], CV_32SC1);
	}

	void readFirstFrames(VideoCapture& fs, const int n_frames, const float aoiSz, vector<Mat>& frame_array)
	{

		for (int i = 0; i < n_frames; i++)
		{
			Mat frame;
			fs >> frame;
			frame_array.push_back(changeFrameSize(frame, aoiSz));
		}
		return;
	}


	void readNextFrames(VideoCapture& fs, const int n_frames, const float aoiSz, vector<Mat>& frames_array)
	{
		for (int i = frames_array.size(); i < n_frames; i++)
		{
			Mat frame;
			fs >> frame;
			frames_array.push_back(changeFrameSize(frame, aoiSz));
		}
	}

	Mat ceateRPMask(const vector<Mat> &bgr32SC1)
	{
		const float alpha = 3.0;
		//r = pic(:, : , 1) / 3;
		//g = pic(:, : , 2) / 3;
		//b = pic(:, : , 3) / 3;
		
		Mat r, g, mask;
		
		r = bgr32SC1[2] / alpha;
		g = bgr32SC1[1] / alpha;

		//% s = r + g + b;
		//% gRatio = s. / g;
		//% rRatio = s. / r;

		//mask = r-g > 15;
		mask = (r - g) > 15;
		return mask;
	}

	Mat ceateRPMask(const Mat&frame)
	{
		const float alpha = 3.0;
		//r = pic(:, : , 1) / 3;
		//g = pic(:, : , 2) / 3;
		//b = pic(:, : , 3) / 3;
		vector<Mat> channels(3);
		Mat r, g, b, mask;
		split(frame, channels);
		
		channels[2].convertTo(r, CV_32SC1);
		channels[1].convertTo(g, CV_32SC1);
		r /= alpha;
		g /= alpha;

		//% s = r + g + b;
		//% gRatio = s. / g;
		//% rRatio = s. / r;

		//mask = r-g > 15;
		mask = (r - g) > 15;
		return mask;
	}
	Mat createCMask(const Mat &frame)
	{
		vector<Mat> channels(3);
		Mat b, gbToAllDiv, mask,g,r;
		Mat s = Mat::zeros(frame.size(), CV_32FC1);
		const float alpha = 3.0;
		const double bgRatio = 0.7;
		split(frame, channels);
		//matlab: b = pic(:,:,2) / 3;
		channels[0].convertTo(b, CV_32FC1);
		channels[1].convertTo(g, CV_32FC1);
		channels[2].convertTo(r, CV_32FC1);

//		b /= alpha;
		//matlab: s = pic(:,:,1)/3+pic(:,:,2)/3+b;
		auto it = channels.begin();
		Mat gb = g+b;
		s = r + gb;
		// s = channels[2] / alpha + channels[1] / alpha + channels[0] / alpha;
		// bRatio = s / b;
		divide(gb, s, gbToAllDiv);

		//matlab: mask = bRatio < ratioThr & b > 50;
		//TODO - make sure matrices are in range [0 255] - if not - just keep add /250
		if (true)
			mask = gbToAllDiv > bgRatio & b > 155 & g > 155;
		else {
//			threshold(bRatio, bRatio, ratioThr, 255.0, THRESH_BINARY_INV);
//			threshold(b, b, 50, 255.0, THRESH_BINARY);
//			bitwise_and(bRatio, b, mask);
//			morphologyEx(mask, mask, MORPH_CLOSE, getStructuringElement(CV_SHAPE_ELLIPSE, Size(3, 3)));
//			mask.convertTo(mask, CV_8UC1);
		}
		return mask;
	}
	Mat createBMask(const vector<Mat> &bgr32SC1)
	{
		Mat bRatio, mask;
		Mat s = Mat::zeros(bgr32SC1[0].size(), CV_32SC1);
		const float alpha = 3.0;
		Mat b = bgr32SC1[0]/alpha;
		//matlab: s = pic(:,:,1)/3+pic(:,:,2)/3+b;

		s += bgr32SC1[1];
		s += bgr32SC1[2];
		s /= alpha;
		s += b;
		// s = channels[2] / alpha + channels[1] / alpha + channels[0] / alpha;
		// bRatio = s / b;
		divide(s, b, bRatio);
		//bRatio.convertTo(bRatio, CV_32S, 1, 0.5);

		//matlab: mask = bRatio < ratioThr & b > 50;
		if (true)
			mask = bRatio < ratioThr & b > 50;
		else {
			threshold(bRatio, bRatio, ratioThr, 255.0, THRESH_BINARY_INV);
			threshold(b, b, 50, 255.0, THRESH_BINARY);
			bitwise_and(bRatio, b, mask);
			morphologyEx(mask, mask, MORPH_CLOSE, getStructuringElement(CV_SHAPE_ELLIPSE, Size(3, 3)));
			mask.convertTo(mask, CV_8UC1);
		}
		return mask;
	}

	Mat createBMask(const Mat &frame)
	{
		vector<Mat> channels(3);
		Mat b, bRatio, mask;
		Mat s = Mat::zeros(frame.size(), CV_32SC1);
		const float alpha = 3.0;
		split(frame, channels);
		//matlab: b = pic(:,:,2) / 3;
		channels[0].convertTo(b, CV_32SC1);
		b /= alpha;
		//matlab: s = pic(:,:,1)/3+pic(:,:,2)/3+b;

		auto it = channels.begin();
		//We can skip first 'b' channel - already converted..
		for (++it; it != channels.end(); ++it)
		{
			Mat tmp;
			it->convertTo(tmp, CV_32SC1);
			s += tmp;
		}
		
		s /= alpha;
		s += b;
		// s = channels[2] / alpha + channels[1] / alpha + channels[0] / alpha;
		// bRatio = s / b;
		divide(s, b, bRatio);
		//bRatio.convertTo(bRatio, CV_32S, 1, 0.5);

		//matlab: mask = bRatio < ratioThr & b > 50;
		if (true)
			mask = bRatio < ratioThr & b > 50;
		else {
			threshold(bRatio, bRatio, ratioThr, 255.0, THRESH_BINARY_INV);
			threshold(b, b, 50, 255.0, THRESH_BINARY);
			bitwise_and(bRatio, b, mask);
			morphologyEx(mask, mask, MORPH_CLOSE, getStructuringElement(CV_SHAPE_ELLIPSE, Size(3, 3)));
			mask.convertTo(mask, CV_8UC1);
		}
		return mask;
	}

	Mat createRMask(const vector<Mat> &bgr32SC1)
	{
		Mat r, rRatio, mask;
		Mat s = Mat::zeros(bgr32SC1[0].size(), CV_32SC1);
		const float alpha = 3.0;
		double redRatioThr = 3.0;
		//matlab: r = pic(:, :, 1) / 3;
		r = bgr32SC1[2]/alpha;
		s += bgr32SC1[1] + bgr32SC1[0];
		s /= alpha;
		s += r;
		// s = channels[2] / alpha + channels[1] / alpha + channels[0] / alpha;
		// rRatio = s / r;
		divide(s, r, rRatio);
		//rRatio.convertTo(rRatio, CV_8U, 1, 0.5);
		//matlab: mask = bRatio<ratioThr & b > 50;
		if (true)
			mask = rRatio < redRatioThr & r > 50;
		else {
			threshold(rRatio, rRatio, redRatioThr, 255.0, THRESH_BINARY_INV);
			threshold(r, r, 50, 255.0, THRESH_BINARY);
			bitwise_and(rRatio, r, mask);
			morphologyEx(mask, mask, MORPH_CLOSE, getStructuringElement(CV_SHAPE_ELLIPSE, Size(3, 3)));
			mask.convertTo(mask, CV_8UC1);
		}
		return mask;
	}

	Mat createRMask(const Mat &frame)
	{
		vector<Mat> channels(3);
		Mat r, rRatio, mask;
		Mat s = Mat::zeros(frame.size(), CV_32SC1);
		const float alpha = 3.0;
		double redRatioThr = 3.0;
		split(frame, channels);
		//matlab: r = pic(:, :, 1) / 3;
		channels[2].convertTo(r, CV_32SC1);
		r /= alpha;
		//r = channels[2] / alpha;
		//matlab: s = pic(:,:,1)/3+pic(:,:,2)/3+b;
		//for (auto it = channels.begin(); it != channels.end(); ++it)
		for (int i=0; i<channels.size()-1; ++i) //work on B and G - then add R (already calculated above)
		{
			Mat tmp;
			channels[i].convertTo(tmp, CV_32SC1);// CV_32FC1
			s += tmp;
		}
		s /= alpha;
		s += r;
		// s = channels[2] / alpha + channels[1] / alpha + channels[0] / alpha;
		// rRatio = s / r;
		divide(s, r, rRatio);	
		//rRatio.convertTo(rRatio, CV_8U, 1, 0.5);
		//matlab: mask = bRatio<ratioThr & b > 50;
		if (true)
			mask = rRatio < redRatioThr & r > 50;
		else {
			threshold(rRatio, rRatio, redRatioThr, 255.0, THRESH_BINARY_INV);
			threshold(r, r, 50, 255.0, THRESH_BINARY);
			bitwise_and(rRatio, r, mask);
			morphologyEx(mask, mask, MORPH_CLOSE, getStructuringElement(CV_SHAPE_ELLIPSE, Size(3, 3)));
			mask.convertTo(mask, CV_8UC1);
		}
		return mask;
	}

	Mat cleanMask(const cv::Mat& mask)
	{
		//matlab: bw = bwlabel(mask);
		Mat labels, stats, centroids;
		int nLabels = connectedComponentsWithStats(mask, labels, stats, centroids, 8, CV_32S);

		//matlab: rp = regionprops(bw, 'area', 'MajorAxisLength', 'MinorAxisLength');
		//matlab: axesRatio = cat(1,rp(:).MinorAxisLength)./cat(1,rp(:).MajorAxisLength');
		//matlab: f = find(axesRatio > axesRatioThr);
		vector<int> f;

		// Mat label_image = Mat::zeros(Size(mask.cols, mask.rows), CV_8UC1);
		Mat label_image = Mat::zeros(mask.size(), CV_8UC1);

		for (int i = 1; i < nLabels; i++)
		{
			float axesRatio;

			if (stats.at<int>(i, CC_STAT_AREA) >= minArea)
			{
				vector<Point> blobPoints;
				findNonZero(labels == i, blobPoints);
				RotatedRect ellipse = fitEllipse(blobPoints);
				float minorAxisLength = min(ellipse.size.height, ellipse.size.width);
				float majorAxisLength = max(ellipse.size.height, ellipse.size.width);
				axesRatio = minorAxisLength / majorAxisLength;
			}
			else if (stats.at<int>(i, CC_STAT_AREA) == 1)
				axesRatio = 1;
			else if (stats.at<int>(i, CC_STAT_AREA) == 2)
				axesRatio = 2;
			else
				axesRatio = 1;


			if (axesRatio > 0.2)
				f.push_back(i);
		}

		for (size_t i = 0; i < f.size(); i++)
			label_image |= labels == f[i];

		return label_image;

		////matlab: bw = bwlabel(mask);
		//Mat labels, stats, centroids;
		//int nLabels = connectedComponentsWithStats(mask, labels, stats, centroids, 8, CV_32S);

		////matlab: rp = regionprops(bw, 'area', 'MajorAxisLength', 'MinorAxisLength');
		////matlab: axesRatio = cat(1,rp(:).MinorAxisLength)./cat(1,rp(:).MajorAxisLength');
		////matlab: f = find(axesRatio > axesRatioThr);
		//vector<int> f;

		//// Mat label_image = Mat::zeros(Size(mask.cols, mask.rows), CV_8UC1);
		//Mat label_image = Mat::zeros(mask.size(), CV_8UC1);

		//for (int i = 1; i < nLabels; i++)
		//{
		//	float statsWidth = stats.at<int>(i, CC_STAT_WIDTH);
		//	float statsHeight = stats.at<int>(i, CC_STAT_HEIGHT);

		//	float axesRatio = statsWidth / statsHeight;

		//	if (abs(axesRatio - 1.0) < 0.2 && stats.at<int>(i, CC_STAT_AREA) > minArea)
		//	f.push_back(i);
		//}
		//for (size_t i = 0; i < f.size(); i++)
		//{
		//	Mat tempMat = Mat::zeros(mask.size(), CV_8UC1);
		//	compare(labels, f[i], tempMat, CMP_EQ);
		//	bitwise_or(tempMat, label_image, label_image);
		//}
		//threshold(label_image, label_image, 0, 255, CV_THRESH_BINARY);
		//return label_image;
	}
	Mat removeBigBlobs(const Mat mask, double maxBlobSz)
	{
		//matlab: bw = bwlabel(mask);
		Mat labels, stats, centroids;
		int nLabels = connectedComponentsWithStats(mask, labels, stats, centroids, 8, CV_32S);

		//matlab: rp = regionprops(bw, 'area', 'MajorAxisLength', 'MinorAxisLength');
		//matlab: axesRatio = cat(1,rp(:).MinorAxisLength)./cat(1,rp(:).MajorAxisLength');
		//matlab: f = find(axesRatio > axesRatioThr);

		vector<double> statsWidth(nLabels - 1), statsHeight(nLabels - 1);
		vector<double> axesRatio(nLabels - 1);
		vector<int> f;
		// int j = 0;

		Mat label_image = Mat::zeros(mask.size(), CV_8UC1);

		for (int i = 1; i < nLabels; i++)
		{
			if (stats.at<int>(i, CC_STAT_AREA) < maxBlobSz)
			{
				// j++;
				f.push_back(i);

			}
		}
		for (size_t i = 0; i < f.size(); i++)
		{
			Mat tempMat = Mat::zeros(mask.size(), CV_8UC1);
			// int  p = i;
			compare(labels, f[i], tempMat, CMP_EQ);
			bitwise_or(tempMat, label_image, label_image);
		}
		threshold(label_image, label_image, 0, 255, CV_THRESH_BINARY);
		return label_image;
	}
	
	//OMP is a bit faster - but perhaps saving the converted channels and reuse will be faster
	Mat detectLedsTemporal2(vector<Mat> &framesArr)
	{
		Size frame_size = framesArr[0].size();
		Mat mask = Mat::zeros(frame_size, CV_8UC1);
		vector<Mat> channelsCache;
		for (uint i = 0; i < min(uint(framesArr.size() - 1), uint(8)); i++)
		{
			Mat b1, rp2;
			vector<Mat> channels_i;
			if (channelsCache.empty())
			{
				splitFrame2Channels32SC1(framesArr[i], channels_i, 1, 1, 1);
				b1 = createBMask(channels_i);
			}
			else
				b1 = createBMask(channelsCache);
			
			splitFrame2Channels32SC1(framesArr[i+1], channelsCache,1,1,1);
			rp2 = ceateRPMask(channelsCache);
		
			// b1 = createBMask(framesArr[i]);
			//rp2 = ceateRPMask(framesArr[i + 1]);

			//Mat rp2;
			//subtract(framesArr[i + 1], framesArr[i], rp2);
			//vector<Mat> channels(3);
			//split(rp2, channels);
			//rp2 = channels[2];
			//// cvtColor(rp2, rp2, CV_BGR2GRAY);
			//threshold(rp2, rp2, redIncThr + 1, 255.0, THRESH_BINARY);
			//matlab: mask = mask | (b1& rp2);
			bitwise_and(b1, rp2, rp2);
			// tempMat += rp2;
			// bitwise_or(tempMat, mask, mask);
			bitwise_or(rp2, mask, mask);
		}

		//morphologyEx(mask, mask, MORPH_OPEN, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
		return mask;
	}



	Mat detectLedsTemporal(vector<Mat> &framesArr)
	{
		Size frame_size = framesArr[0].size();
		Mat mask = Mat::zeros(frame_size, CV_8UC1);

		//Pre-calcualte mask on all first frames. 
		//This will add only 2 more mask computation, as in original code we already move on most frames twice
		vector<Mat> bMasks(framesArr.size()), rMasks(framesArr.size());

#ifdef OMP
#pragma omp parallel for
		for (int i = 0; i < framesArr.size(); i++)
#else
		for (uint i = 0; i < uint(framesArr.size()); i++)
#endif
		{
			bMasks[i] = createCMask(framesArr[i]);
			rMasks[i] = ceateRPMask(framesArr[i]);
		}
#ifdef OMP
#pragma omp parallel for
		for (int i = 0; i < min(int(framesArr.size() - 1), int(8)); i++)
#else
		for (uint i = 0; i < min(uint(framesArr.size() - 1), uint(8)); i++)
#endif
		{
			/*vector<Mat> channels1,channels2;
			splitFrame2Channels32SC1(framesArr[i], channels1,1,1,1);
			splitFrame2Channels32SC1(framesArr[i+1], channels2,0,1,1);*/

			//Mat b1 = createBMask(framesArr[i]);
			Mat framei = framesArr[i];
			Mat framei2 = framesArr[i+1];

			Mat b1 = bMasks[i].clone();//createCMask (framesArr[i]);
			Mat rp2 = rMasks[i+1].clone();//ceateRPMask(framesArr[i + 1]);
			//Mat b1 = createBMask(channels1);
			//Mat rp2 = ceateRPMask(channels2);
			Mat b2 = bMasks[i+1].clone();//createCMask (framesArr[i]);
			Mat rp1 = rMasks[i].clone();//ceateRPMask(framesArr[i + 1]);

			//Mat rp2;
			//subtract(framesArr[i + 1], framesArr[i], rp2);
			//vector<Mat> channels(3);
			//split(rp2, channels);
			//rp2 = channels[2];
			//// cvtColor(rp2, rp2, CV_BGR2GRAY);
			//threshold(rp2, rp2, redIncThr + 1, 255.0, THRESH_BINARY);
			//matlab: mask = mask | (b1& rp2);
			bitwise_and(b1, rp2, rp2);//blue to red
			bitwise_and(rp1, b2, b2); //red to blue 
			bitwise_or(b2, rp2, rp2); //red->blue OR blue->red
			// tempMat += rp2;
			// bitwise_or(tempMat, mask, mask);
			bitwise_or(rp2, mask, mask);
		}

		//morphologyEx(mask, mask, MORPH_OPEN, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
		return mask;
	}

	Mat detectLedTemporal(vector<Mat> &framesArr)
	{
		Size frame_size = framesArr[0].size();
		Mat mask = Mat::zeros(frame_size, CV_8UC1);
#ifdef OMP
#pragma omp parallel for
		for (int i = 0; i < min(int(framesArr.size() - 1), int(8)); i++)
		{
			Mat frameMask;
			Mat frameIsRedBlue;
			Mat frameIsRedOrange;
			Mat frameHsv;
#else
		Mat frameMask;
		Mat frameIsRedBlue;
		Mat frameIsRedOrange;
		Mat frameHsv;
		for (uint i = 0; i < min(uint(framesArr.size() - 1), uint(8)); i++)
		{
#endif
		
		
			// Reset the frame mask
			frameMask = Mat::zeros(frame_size, CV_8UC1);
			// Convert the BGR frame to an HSV frame
			cvtColor(framesArr[i], frameHsv, CV_BGR2HSV);
			// Check if colors is blue, purple or red, mark such pixels as LEDish
			inRange(frameHsv, Scalar(97.5, 200, 100), Scalar(180, 255, 255), frameIsRedBlue);
			inRange(frameHsv, Scalar(0, 200, 100), Scalar(7.5, 255, 255), frameIsRedOrange);
			frameMask = frameIsRedBlue | frameIsRedOrange;
			blur(frameMask, frameMask, Size(3, 3));
			// Hole fill in map.
			morphologyEx(frameMask, frameMask, MORPH_CLOSE, getStructuringElement(MorphShapes::MORPH_ELLIPSE, Size(5, 5)));
			vector<vector<Point> > contours;
			vector<Vec4i> hierarchy;
			findContours(frameMask, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
			drawContours(frameMask, contours, -1, 255, -1);
			// Use a distance transform to find the led pixel furthest from a non LED pixel.
			distanceTransform(frameMask, frameMask, DistanceTypes::DIST_L2, 5);
			// Grab the blob the peak value belongs to
			// Or the mask into the temporal mask
			mask |= frameMask > 0;
		}
		return mask;
	}

	Point locateLineAOI(const Mat& vec)
	{
		int thr = 1;
		int gap = 100;

		int begAOI, endAOI;
		Point out;

		//matlab: begAOI = find(vec >= thr, 1,'first');  begAOI = max(begAOI - gap, 1);
		for (int i = 0; i < vec.cols; i++)
		{
			if (vec.at<int>(i) >= thr)
			{
				begAOI = i;
				if ((begAOI - gap) > 1)
					begAOI = begAOI - gap;
				else
					begAOI = 0;
				break;
			}
			begAOI = 0;
		}
		//matlab: endAOI = find(vec >= thr, 1,'last'); endAOI = min(endAOI + gap, vecLen);	
		for (int i = vec.cols - 1; i >= 0; i--)
		{
			if (vec.at<int>(i) >= thr)
			{
				endAOI = i;
				if (endAOI + gap > vec.cols - 1)
					endAOI = vec.cols - 1;
				else
					endAOI = endAOI + gap;
				break;
			}
			endAOI = vec.cols - 1;
		}
		out.x = begAOI;
		out.y = endAOI;
		return out;
	}

	void cropMask(const Mat& mask, Mat& cMask, Point& beRow, Point& beCol)
	{
		int rows = mask.rows;
		int cols = mask.cols;
		//matlab: sr = sum(mask,2);  sc = sum(mask);
		Mat sr = Mat(rows, 1, CV_32SC1);
		Mat sc = Mat(1, cols, CV_32SC1);

		reduce(mask, sr, 0, CV_REDUCE_SUM, CV_32SC1);
		reduce(mask, sc, 1, CV_REDUCE_SUM, CV_32SC1);

		transpose(sc, sc);

		beRow = locateLineAOI(sr);  // begin and end row
		beCol = locateLineAOI(sc);  // begin and end col

		Rect ROI = Rect(beRow.x, beCol.x, beRow.y - beRow.x + 1, beCol.y - beCol.x + 1);

		cMask = mask(ROI).clone();
		return;
	}

	// Calculate hough map of given img_data mask with given width w and height h
	// Output map size is given in houghImageH and houghImageW(=180)
	// Size is set according to matlab formulation:  2*ceil(norm( size(I) - floor( (size(I)-1) /2 )-1))+3
	vector<int> calcHoughMap(unsigned char* img_data, int w, int h, vector<int>& angles, double& houghImageH, double&houghImageW)
	{
		double _img_w = w;
		double _img_h = h;
		

		//Create the accu  
		double hough_h = ((sqrt(2.0) * (double)(h > w ? h : w)) / 2.0);
		 houghImageH = hough_h * 2.0; // -r -> +r  
		
		 //Matlab formula: 2*ceil(norm( size(I) - floor( (size(I)-1) /2 )-1))+3
		 auto norm1st = (h - floor((h - 1) / 2) - 1);
		 auto norm2nd = (w - floor((w - 1) / 2) - 1);
		 auto norm = sqrt(norm1st*norm1st + norm2nd*norm2nd);
		 
		 houghImageH = 2 * ceil(norm) + 3;
		 hough_h = houghImageH / 2;
		 houghImageW = 180;

		//unsigned int* _accu = (unsigned int*)calloc(_accu_h * _accu_w, sizeof(unsigned int));
		vector<int> _accu(int(houghImageW*houghImageH),0);

		double center_x = w / 2;
		double center_y = h / 2;


		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				if (img_data[(y*w) + x] > 250)
				{
					if (angles.size() == 0)
					{
						for (int t = 0; t < 180; t++)
						{
							double r = (((double)x - center_x) * cos((double)t * DEG2RAD)) + (((double)y - center_y) * sin((double)t * DEG2RAD));
							//Omri - we crash with matlab formula for size. check size...
							//if ((int)((round(r + hough_h) * 180.0)) + t <_accu.size())
							_accu[(int)((round(r + hough_h) * 180.0)) + t]++;
						}
					}
					else
					{
							for (int ii = 0; ii < angles.size(); ++ii)
							{
								int t = angles[ii];
								double r = (((double)x - center_x) * cos((double)t * DEG2RAD)) + (((double)y - center_y) * sin((double)t * DEG2RAD));
								//Omri - we crash with matlab formula for size. check size...
								//if ((int)((round(r + hough_h) * 180.0)) + t <_accu.size())
								_accu[(int)((round(r + hough_h) * 180.0)) + t]++;
							}
					}
				}
			}
		}

		return _accu;
	}


	void detectLedsLineByRadon(const Mat& mask, const Point& imCenterTrans, float& slope, float& offset, float& estAngle)
	{
		vector<Vec4i> lines;
//		double minLineLength = mask.cols > mask.rows ? 0.1 * double(mask.rows) : 0.1 * double(mask.cols);
//		//double minLineLength = mask.cols > mask.rows ? 0.5 * double(mask.rows) : 0.5 * double(mask.cols);
//		// TODO: tune parameters of hough algorithm
//		HoughLinesP(mask, lines, 1, CV_PI / 180, 10, minLineLength, 100);
//
//		// plot found lines
//#ifdef DEBUG
//		if (debug & false)
//		{
//			cout << lines.size() << endl;
//			Mat cdst;
//			cvtColor(mask, cdst, CV_GRAY2BGR);
//			for (auto it = lines.begin(); it != lines.end(); ++it)
//				line(cdst, Point((*it)[0], (*it)[1]), Point((*it)[2], (*it)[3]), Scalar(0, 0, 255), 1, CV_AA);
//			imshow("detectLedsLineByRadon: lines", cdst);
//			waitKey();
//		}
//#endif
//		if (!lines.size())
//			throw AlgorithmError("no leds found");
//		// we found mamy parallel lines, we take average one
//		Vec4f avg_line(0, 0, 0, 0);
//		for (auto it = lines.begin(); it != lines.end(); ++it)
//			avg_line += ((Vec4f)*it) / (float)lines.size();
//		Point lle((int)round(avg_line[0]), (int)round(avg_line[1]));  // line left end
//		Point lre((int)round(avg_line[2]), (int)round(avg_line[3]));  // line right end									  // plot average line
//#ifdef DEBUG
//		if (debug & false)
//		{
//			Mat cdst;
//			cvtColor(mask, cdst, CV_GRAY2BGR);
//			line(cdst, lle, lre, Scalar(0, 0, 255), 1, CV_AA);
//			imshow("detectLedsLineByRadon: detected line", cdst);
//			waitKey();
//		}
//#endif
//		float dx = avg_line[2] - avg_line[0];
//		float dy = avg_line[3] - avg_line[1];
//		slope = dy / dx;
//
//		float oppositeangle = atan2(-dy, dx) * 180 / CV_PI;
//		estAngle = 90 + oppositeangle;
//		offset = avg_line[1] + imCenterTrans.x - slope * (avg_line[0] + imCenterTrans.y);
//

		//Omri - test slopes and offsets
		float oppositeangle;
		double houghRows;
		double houghCols;
		size_t maxIdx = 0;
		unsigned int maxVal = 0;
		
	
		vector<int> angles(40);
		iota(angles.begin(), angles.begin() + 10, 0);
		iota(angles.begin()+10, angles.begin() + 20, 170);
		iota(angles.begin() + 20, angles.begin() + 40, 80);
		
		//Timer timer(true);
		// Calculate the hue thresholds

		vector<int> myh = calcHoughMap(mask.data, mask.cols, mask.rows, angles, houghRows, houghCols);
		//std::cout << "hough with angles: " << std::fixed << timer << "ms\n";
#ifdef DEBUG	
		if (debug)
		{
			//Mat A = Mat::eye(11, 11, CV_8UC1) * 255;
			//Mat A = Mat::zeros(mask.cols, mask.cols, CV_8UC1) * 255;
			//line(A, Point( 0, A.rows / 2), Point(A.cols, A.rows / 2), 255);//horiznotal
			//line(A, Point(A.cols / 2,0 ), Point(A.cols/2, A.rows ), 255); //vertical
			//line(A, Point(4, 0), Point(A.cols , A.rows-4), 255); 
			//line(A, Point(0, 4), Point(A.cols-4, A.rows), 255); 
			//line(A, Point(4, A.rows), Point(A.cols, 4), 255); 
			//unsigned int* myh = myHough(mask.data, mask.cols, mask.rows, houghRows, houghCols);
			//myh = myHough(A.data, A.cols, A.rows, angles, houghRows, houghCols);
			//Mat M(houghRows, houghCols, CV_32S, Scalar(0));
			//for (size_t i = 0; i <houghRows*houghCols; ++i)
			//{
			//	reinterpret_cast<int*>(M.data)[i] = myh[i];
			//}
		}
#endif
		maxIdx = distance(myh.begin(), max_element(myh.begin(), myh.end()));

		size_t distInd = maxIdx / houghCols;
		size_t angInd = maxIdx % (int)houghCols;
		angInd = convertHoughToMatlabRadon(angInd);
		//angInd = (180 - angInd) % 180;

		/*	int temp = floor(houghRows);
			vector<int>  px(temp, 0);
			for (int i = -ceil(temp / 2), j = 0; j < px.size(); ++j, ++i)
				px[j] = i;*/


		//%dist = px(distInd);
		//auto dist = px[distInd];
		auto dist = round(houghRows / 2 - distInd);
		//%opositeAngle = refAngle + 90;
		oppositeangle = angInd + 90;
		//%deltaX = dist*sind(opositeAngle);
		auto deltax = dist*sin(oppositeangle*DEG2RAD);
		//%deltaY = dist*cosd(opositeAngle);
		auto deltay = dist*cos(oppositeangle*DEG2RAD);

		//%x = maskSz(2) / 2 + imCenterTrans(1) + deltaX;
		auto x = mask.cols / 2 + imCenterTrans.y + deltax;
		//%y = maskSz(1) / 2 + imCenterTrans(2) + deltaY;
		auto y = mask.rows / 2 + imCenterTrans.x + deltay;

		//%slope = -tand(opositeAngle);
		float slope2 = -tan(oppositeangle*DEG2RAD);
		//%offset = y - slope *x;
		float offset2 = y - slope2*x;

		slope = slope2;
		offset = offset2;
		estAngle = angInd ;
		return;
	}

	vector<float> cutLineVecFromIm(const Mat& im, const float slope, const float offset)
	{
		vector<float> v;
		if (abs(slope) < 0.5) {
			for (int x = 0; x < im.cols; x++) {
				int y = int(offset + slope * x);
				if (y >= 0 && y < im.rows)
				{
					v.push_back(im.at<uchar>(y, x));
				}
			}
		}
		else {
			for (int y = 0; y < im.rows; y++) {
				int x = int(((double)y - offset) / slope);
				if (x >= 0 && x < im.cols)
				{
					v.push_back(im.at<uchar>(y, x));
				}
			}
		}
		return v;
	}

	vector<float> extract_rb_Line(const Mat& im, const float slope, const float offset)
	{
		// extract rbLine from rLine and bLine
		vector<float> rLine, gLine, bLine, rbLine;  // float???
		vector<Mat> channels;

		split(im, channels);

		// matlab: rLine(cutLineVecFromIm(im(:,:,1), slope, offset);
		// matlab: bLine(cutLineVecFromIm(im(:,:,3), slope, offset);
		rLine = cutLineVecFromIm(channels[2], slope, offset);
		bLine = cutLineVecFromIm(channels[0], slope, offset);
        gLine = cutLineVecFromIm(channels[1], slope, offset);


        //get the maximum value from rLine and bLine
		for (size_t i = 0; i < rLine.size(); i++)
		{
            //matlab - rbLine= max(rLine, (bLine+gLine)/2 );
            float meanGb = (bLine[i] + gLine[i])/2;
			int m = meanGb > rLine[i] ? meanGb : rLine[i];
			rbLine.push_back(m);
		}
		return rbLine;
	}

	double detectPitch(const Mat& X)
	{
		int minMaxFLoc = 4;	// minimal peak frequency
		int Fs = 1;  // Sampling frequency
		int L = X.rows;  // Length of signal

		X.convertTo(X, CV_32FC1);  // does it needed?
								   // TODO: what about optimal size?
		int optDFTsize = getOptimalDFTSize(L);
		Mat Y;  // frequence
		dft(X, Y, DFT_COMPLEX_OUTPUT);

		Mat planes[] = { Mat::zeros(X.rows, 1, CV_32F), Mat::zeros(X.rows, 1, CV_32F) };
		Mat spectrum;
		split(Y, planes);  // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
		magnitude(planes[0], planes[1], spectrum);
		float * row_ptr = spectrum.ptr<float>(0);
		// find the frequence peak
		float freq_value = 0;
		double dt = 0;
		for (int i = minMaxFLoc; i < int(L / 2); i++) {
			if (row_ptr[i] > freq_value)
			{
				freq_value = row_ptr[i];
				dt = double(L) / double(Fs*i);
			}
		}
		// cout << dt << endl;
		// plot the spectrum
		// auto p = spectrum.ptr<float>(0);
		// vector<float> arr(p, p + spectrum.rows);
		// int range[2] = {0, 300};
		// Mat lineGraph = plotGraph(arr, range);
		// cv::imshow("", lineGraph);
		// waitKey();
		return dt;
	}

	void estimatePitchByFFT(const Mat& im, const Mat& mask, const float slope, const float offset, float &pitch, vector<Point>& cutXY)
	{
		vector<float> rbLine;
		//Mat copMask = mask.clone();
		//extract line of blue and red
		rbLine = extract_rb_Line(im, slope, offset);
		
		//int range[2] = {0, 300};
		//cv::Mat lineGraph = plotGraph(rbLine, range);
		//imshow("estimage pitch - line graph", lineGraph);
		//waitKey();
		
		//Omri
		Mat masktemp = mask.clone();

		vector<int> l;

		//TODO - this is the matlab logic from cutLineVecFromIm - I think they missed that.
		//Need to implement this logic here 
		//if abs(slope) < 0.5
		//	x = 1:imSz(2);
		//y = round(x*slope + offset);
		//else
		//	y = 1:imSz(1);
		//x = round((y - offset) / slope);
		//end

		//	vector<Point> xy;
		if (abs(slope) < 0.5)
		{
			for (int x = 0; x < mask.cols; x++)
			{
				int y = int(double(x) * slope + offset);// +1;
				
				//omri debug
				if (y >= 0 && y < mask.rows)
					masktemp.at<uchar>(y, x) = 120;


				if (y >= 0 && y < mask.rows && mask.at<uchar>(y, x) > 0)
				{
					l.push_back(x); //TODO- need to push back here the linear idx - not just x...
					//l.push_back(y*mask.cols + x);
				}
				//		xy.push_back(Point(x,y));
			}
		}
		else
		{
			for (int y = 0; y < mask.rows; y++)
			{			
				int x = int(((double)y - offset) / slope);//+1;
	
				//omri debug
				if (x >= 0 && x < mask.cols)	
					masktemp.at<uchar>(y, x) = 120;

				if (x >= 0 && x < mask.cols && mask.at<uchar>(y, x) > 0)
				{
					l.push_back(y); //TODO- need to push back here the linear idx - not just x...
					//l.push_back(y*mask.cols + x); //try matlab s
				}
				//		xy.push_back(Point(x,y));
			}
		}

		if (l.empty())
		{
			throw AlgorithmError("Failed to estimatePitchByFFT - mask doesn't match leds line");
		}

		Mat cutV;
		int len = l.at(l.size() - 1) - l.at(0);
		int gap = round(float(len) / 10.0);
		int begV = l.at(0) - gap > 0 ? l.at(0) - gap : 0;

		int endV = l.at(l.size() - 1) + gap < rbLine.size() ? l.at(l.size() - 1) + gap : rbLine.size();
		for (int i = begV; i < endV; i++)
		{
			cutV.push_back(rbLine[i]);
			if (abs(slope) < 0.5)
				cutXY.push_back(Point(i, int(double(i) * slope + offset)));
			else
				cutXY.push_back(Point(int(((double)i - offset) / slope), i));
		}

		pitch = detectPitch(cutV);
		return;
	}

	void cutMaskByPitch(const Mat& mask, const double estPitch, const vector<Point>& cutXY, Mat& cutMask, Point& begPoint, Point& endPoint)
	{
		int x_max = max(cutXY[cutXY.size() - 1].x, cutXY[0].x);
		int x_min = min(cutXY[cutXY.size() - 1].x, cutXY[0].x);
		int y_max = max(cutXY[cutXY.size() - 1].y, cutXY[0].y);
		int y_min = min(cutXY[cutXY.size() - 1].y, cutXY[0].y);

		int xRange = x_max - x_min;
		int	yRange = y_max - y_min;

		// int begX, begY, endX, endY;
		double x_factor, y_factor;

		if (xRange > yRange)
		{
			x_factor = 8.0;
			y_factor = 1.0;
		}
		else
		{
			x_factor = 1.0;
			y_factor = 8.0;
		}

		begPoint.x = (int)round(max(0.0, double(x_min) - x_factor * estPitch));
		begPoint.y = (int)round(max(0.0, double(y_min) - y_factor * estPitch));
		endPoint.x = (int)round(min(double(x_max) + x_factor * estPitch, double(mask.cols)));
		endPoint.y = (int)round(min(double(y_max) + y_factor * estPitch, double(mask.rows)));

		Rect rect(begPoint.x, begPoint.y, endPoint.x - begPoint.x, endPoint.y - begPoint.y);
		cutMask = mask(rect).clone();
		// imshow("cutMask",cutMask);
		// waitKey();
		return;
	}

	template<typename T>
	vector<T> vecDiff(const vector<T>& vec)
	{
		vector<T> diffVec(vec.size() - 1);
		T v1 = vec[0];
		for (size_t i = 1; i < vec.size(); i++)
		{
			T v2 = vec[i];
			diffVec[i - 1] = v2 - v1;
			v1 = v2;
		}
		return diffVec;
	}

	vector<float> findLocalMax1D(const vector<float>& vec)
	{
		vector<float> diffVec = vecDiff(vec);
		//matlab: locMax = find(diffVec<0 & [0; diffVec(1:end-1)] >=0);
		//matlab: locMax = setdiff(locMax, [1 length(vec)]);
		vector<float> locMax;
		int glThr = 25;
		//for (size_t i = 1; i < diffVec.size() - 1; i++)
		for (size_t i = 1; i < diffVec.size(); i++)
		{
			//if (diffVec[i - 1] > 0 && diffVec[i] <= 0 && vec[i] > glThr)
			//Omri - condition seems not identical to matlab...
			if (diffVec[i] < 0 && diffVec[i-1] >= 0 && vec[i] > glThr)
				locMax.push_back(i);
		}
		return locMax;
	}

	bool validPeaks(const vector<float>& vec, const vector<float>& peaksLocs)
	{
		//% drop of maximum half peak height between one peak to the next
		vector<float> peaksHeight(peaksLocs.size());// = vec at peaksLocs
		for (int i = 0; i < peaksLocs.size(); ++i)
			peaksHeight[i] = vec[(int)peaksLocs[i]];

		//%	d = diff(peaksHeight);
		vector<float> d = vecDiff<float>(peaksHeight);
		//%valid = max(2 * d. / (peaksHeight(1:end - 1) + peaksHeight(2:end))) < 0.5;

		//TODO - what if d is negative? this will always be valid, but d can be large negative..
		bool valid = true;

		for (int i = 0; i < peaksHeight.size()-1; ++i)
		{
			if (2 * abs(d[i]) / (peaksHeight[i] + peaksHeight[i + 1]) > 0.5)
			{
				valid = false;
				break;
			}
		}

		return valid;
		
	}

	//TODO - add color parameters
	Mat plotRBLineWithCenters(vector<float>& vec, vector<float>& peaksLocs, vector<float>* gauss = NULL, CvScalar color = CV_RGB(255, 0, 0))
	{
		int range[2] = { 0, 300 };
		Mat lineGraph = plotGraph(vec, range);
		for (int i = 0; i < peaksLocs.size(); ++i)
			line(lineGraph, Point(peaksLocs[i], 0), Point(peaksLocs[i], 300), color, 1);

		
		if (gauss)
		{
			Mat gaussG = plotGraph(*gauss, range, CV_RGB(0, 255, 0));
			lineGraph = lineGraph + gaussG;
		}

		//cv::imshow("rbLine", lineGraph);
		//waitKey();
		return lineGraph.clone();

	}

	//a - gaussian height
	//b - mean
	//c - sigma //std
	float estimateGaussAt(float ampl, float mu, float sigma, float x)
	{
		//The parameter a is the height of the curve's peak, b is the position of the center of the peak and c (the standard deviation, sometimes called the Gaussian RMS width) controls the width of the "bell".
		//f(x) = a*exp((-(x-b)^2)/2c^2)
		//float res = a*exp(-((x - b)*(x - b)) / (2 * c * c));
		
		//A*1/(sqrt(2*pi*sigma^2))*exp((x-mu).^2/(-2*sigma^2));
		float res = ampl / sqrt(2*CV_PI*sigma*sigma)*exp( (x - mu)*(x - mu)/(-2*sigma*sigma));
		return res;
	}

	bool isSymPeak(const vector<float>& vec, const vector<float>& peaksLocs, float peakIdx)
	{	

		//			% if first
		//if (peakIdx == 1)
		//					estPitch = peaksLocs(3) - peaksLocs(2);
		//else
		//					estPitch = peaksLocs(peakIdx - 1) - peaksLocs(peakIdx - 2);
		//end

		float estPitch = 0;
		if (peakIdx == 0)
			estPitch = peaksLocs[2] - peaksLocs[1];
		else
			estPitch = peaksLocs[peaksLocs.size() - 2] - peaksLocs[peaksLocs.size() - 3];


		//				peakIdx = peaksLocs(peakIdx);
		peakIdx = peaksLocs[peakIdx];
		//	halfPitch = round(estPitch / 3);
		float delta = round(estPitch / 3);

		//			idx = max(1, peakIdx - halfPitch) :min(peakIdx + halfPitch, length(vec));
		int startIdx = (peakIdx - delta) > 0 ? (peakIdx - delta) : 0;
		int endIdx = (peakIdx + delta) < vec.size() - 1 ? (peakIdx + delta) : vec.size() - 1;
		vector<int> idx(endIdx - startIdx+1);		

		iota(idx.begin(), idx.end(), startIdx);

		//%if first and last points are too far - something is off
//				d1 = vec(peakIdx) - vec(idx(1));
		float d1 = vec[peakIdx] - vec[idx[0]];
			//d2 = vec(peakIdx) - vec(idx(end));
		float d2 = vec[peakIdx] - vec[idx.back()];
			//if (d1 <= 0 || d2 <= 0)
		if (d1 <= 0 || d2 <= 0)
		{
			//				disp('points aroung peak are higher');
						//isSym = false;
			return false;
						//return;
						//end
		}
//				d12 = abs(vec(idx(end)) - vec(idx(1)));
		float d12 = abs(vec[idx.back()] - vec[idx[0]]);

			//if (d12 / min([d1, d2]) > 2)

		if ((d12 / min(d1, d2)) > 2)
		{
			//				disp('vertical distance between end points is x2 than minimal distance to peak')
			//isSym = false;
			//return;
			//end
			return false;
		}

		//midIdx = find(idx == peakIdx);			
		auto midIdxIter = find(idx.begin(), idx.end(), peakIdx);
		auto midIdx = distance(idx.begin(), midIdxIter);

		//f = fit(idx',vec(idx),'gauss1');
		float gaussHeight = vec[idx[midIdx]];

		//for debugging
		vector<float> gaussData(idx.size());
		for (int i = 0; i < idx.size(); ++i)
			gaussData[i] = vec[idx[i]];
		//for debugging

	/*	float sum = std::accumulate(idx.begin(), idx.end(), 0.0);
		float mean = sum / idx.size();
		std::vector<double> diff(idx.size());
		std::transform(idx.begin(), idx.end(), diff.begin(), [mean](double x) { return x - mean; });
		
		float sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
		float stdev = std::sqrt(sq_sum / idx.size());*/

		/*
		Asaf formula:
		estimateMU =(sum(x.*y)/sum(y))
		estimateSigma = sqrt((sum(y.*(x-estimateMU).^2))/sum(y))
		estimateA = sum(y)
		*/
		float mu = 0;
		float sigma = 0;
		float sumy = 0;
		for (int i = 0; i < idx.size(); ++i)
		{
			mu += idx[i] * vec[idx[i]];
			sumy += vec[idx[i]];
		}
		mu = mu / sumy;
		
		for (int i = 0; i < idx.size(); ++i)
		{
			sigma += vec[idx[i]] * (idx[i] - mu)*(idx[i] - mu);
		}
		sigma = sqrt(sigma / sumy);
		float ampl = sumy;
		
			//lowerIdxIdx = 1:midIdx - 1;
			//higherIdxIdx = midIdx + 1:length(idx);
			//
			//ssdErr1 = sqrt(sum((f(idx(lowerIdxIdx)) - vec(idx(lowerIdxIdx))). ^ 2, 1) / length(lowerIdxIdx));
			//ssdErr2 = sqrt(sum((f(idx(higherIdxIdx)) - vec(idx(higherIdxIdx))). ^ 2, 1) / length(higherIdxIdx));
		float ssdErr1 = 0, ssdErr2 = 0;
		int lowerIdxCount = 0, highIdxCount = 0;
		vector<float> gaussVec;
		float rmsd = 0;
		for (int i = 0; i < idx.size(); ++i)
		{
			float gaussVal = estimateGaussAt(ampl, mu, sigma, idx[i]);
			gaussVec.push_back(gaussVal);

			//float d = gaussVal - vec[idx[i]];
			//rmsd += d*d;

			if (idx[i] == peakIdx)
				continue;

			float d = gaussVal - vec[idx[i]];
			
			if (idx[i] < peakIdx)
			{				
				lowerIdxCount += 1;
				ssdErr1 += d*d;
			}
			else //(i > peakIdx)
			{
				highIdxCount += 1;
				ssdErr2 += d*d;
			}
		}
		ssdErr1 = sqrt(ssdErr1 / lowerIdxCount);
		ssdErr2 = sqrt(ssdErr2 / highIdxCount);
		//rmsd = sqrt(rmsd / idx.size());

		//plotting
		 //vector<float> tmp; tmp.push_back(mu);
		//Mat plotRes = plotRBLineWithCenters(gaussData, tmp, &gaussVec);

	//	cout << "rmsd: "<< rmsd<<", ssdErr1: " << ssdErr1 << ", ssdError2: " << ssdErr2 << endl;
		//cout << "abs(ssdErr1 - ssdErr2): " << abs(ssdErr1 - ssdErr2) << endl;
	//float ssdR = ssdErr1 < ssdErr2 ? (ssdErr1 / ssdErr2) : (ssdErr2 / ssdErr1);
		//cout << "Ratio: " << ssdR << endl;
		//%isSym = abs(ssdErr1 - ssdErr2) < 5; %TODO - fix condition if necessary*/

		//Error in matlab are much smaller (probably due to better fit)
		//Cannot use 0.65 ratio there - since with small error - every small movement can cause big ratio (e.g. error 9 and 5 are possible)

		
		//return abs(ssdErr1 - ssdErr2) < 15;
		float ssdR = ssdErr1 < ssdErr2 ? (ssdErr1 / ssdErr2) : (ssdErr2 / ssdErr1);
		cout << "Ratio: " << ssdR << endl;
		return ssdR >= 0.65; //I see some "good" peaks that have ~ 0.66. One bad had ~0.54. So 0.65 seems ok

	}

	template <typename T>
	double calcStd(vector<T> v)
	{
		double sum = std::accumulate(v.begin(), v.end(), 0.0);
		double mean = sum / v.size();

		double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
		double stdev = std::sqrt(sq_sum / v.size() - mean * mean);
		return stdev;
	}

	vector<float> locatePeaks(vector<float>& vec, double estPitch, int nExpLeds)
	{
		int nLeds = 0;
		int iterNum = 0;
		int maxIters = 100;
		vector<float> peaksLocs;
		float maxGap = max(2.0, estPitch / 9.0);
		vector<float> locMax;

		if (vec.empty())
		{
			return peaksLocs;
		}

		while (nLeds < nExpLeds && iterNum < maxIters)
		{
			//Mat matDebug = plotRBLineWithCenters(vec, peaksLocs);
			iterNum++;
			//matlab commented code - rev407:
			//%     locMax = locMax(vec(locMax)  > 0.6 * max(vec(:)));
			locMax = findLocalMax1D(vec);
			if (locMax.size() == nExpLeds)
			{
				peaksLocs = locMax;
				nLeds = peaksLocs.size();
				//plotRBLineWithCenters(vec, peaksLocs);
				//return peaksLocs;
			}
			else if (locMax.size() < nExpLeds)
			{
				peaksLocs = locMax;
				//plotRBLineWithCenters(vec, peaksLocs);
				return peaksLocs;
			}


			// how many local maxes are about 'estimated pitch' far from each other?
			vector<float> dists = vecDiff(locMax);
			vector<float> diffFromEstPitch;
			absdiff(dists, Scalar::all(estPitch), diffFromEstPitch);

			double minVal = numeric_limits<int>::max();
			int f0 = 0;
			for (int i = 0; i < diffFromEstPitch.size(); i++)
			{
				if (diffFromEstPitch[i] < minVal)
				{
					minVal = diffFromEstPitch[i];
					f0 = i;
				}
			}

			//matlab//vec = smooth(vec,3);
			blur(vec, vec, Size(3, 1), Point(-1, -1));

			//minMaxLoc(diffFromEstPitch, &minVal, NULL, &minLoc, NULL);
			//matlab: if(diffFromEstPitch > 3 || f0 == length(diffFromEstPitch))
			//			continue;
			if (minVal > 3.0 || f0 == diffFromEstPitch.size() - 1)
				continue;

			vector<float> diffDists = vecDiff(dists);
			//matlab: f1 = find(abs(diffDiss(1:f0) > maxGap, 1, 'last') + 1;
			int f1 = 0;
			for (int i = f0; i >= 0; i--)
			{
				if (abs(diffDists[i]) > maxGap)
				{
					f1 = i + 1;
					break;
				}
			}
			int f2 = dists.size() - 1;
			//matlab: f2 = find(abs(diffDists(f0: end)) > maxGap, 1, 'first') + f0 - 1 ; % last peak of interest
			for (int i = f0; i < diffDists.size(); i++)
			{
				if (abs(diffDists[i]) > maxGap)
				{
					//f2 = i + 1;
					f2 = i; //Omri - dont add 1 here - because we might not get here. Add one below in forloop
					break;
				}
			}
			f2 = min((int)locMax.size() - 1, f2);
			peaksLocs.clear();
			//for (int i = f1; i <= f2; i++)
			for (int i = f1; i <= f2 + 1; i++) //Omri - missing +1 in f2, now that we dont rely on +1 in previous forloop
				peaksLocs.push_back(locMax[i]);
			nLeds = peaksLocs.size();

#ifdef DEBUG
			if (debug)
				plotRBLineWithCenters(vec, peaksLocs);
#endif
			//Omri - missing Sanity matlab code
			//%% sanity
			//%if nLeds >= nExpLeds && ~validPeaks(vec(peaksLocs)) && iterNum ~= maxIters
			if (nLeds >= nExpLeds && !validPeaks(vec, peaksLocs) && iterNum < maxIters)
			{
				//%	nLeds = 1; % = continue
				nLeds = 1;
				//%	end
			}
		}

		//Mat matDebug = plotRBLineWithCenters(vec, peaksLocs);

		// if there are too many - check the outer locations
		int nAddLeds = nLeds - nExpLeds;
		if (nAddLeds < 0)  // not enough leds were found
		{
			if (locMax.size() == nExpLeds)
				peaksLocs = locMax;
			else
			{
				peaksLocs.clear();
				return peaksLocs;
			}

		}

		while (nAddLeds > 0)
		{
			// vec(peaksLocs)
			vector<float> vec_peaksLocs;
			for (auto it = peaksLocs.begin(); it != peaksLocs.end(); ++it)
				vec_peaksLocs.push_back(vec[*it]);

			//Mat lineGraph = plotRBLineWithCenters(vec, peaksLocs);

				// median(vec(peaksLocs))
			float med;
			sort(vec_peaksLocs.begin(), vec_peaksLocs.end());
			if (vec_peaksLocs.size() % 2 == 0)
				med = vec_peaksLocs.at(vec_peaksLocs.size() / 2);
			else
			{
				med = vec_peaksLocs.at((vec_peaksLocs.size() - 1) / 2);
				med += vec_peaksLocs.at((vec_peaksLocs.size() + 1) / 2);
				med *= 0.5;
			}
			// END: median(vec(peaksLocs))

			float g1 = vec_peaksLocs[0];
			float g2 = vec_peaksLocs[vec_peaksLocs.size() - 1];

			//Fix issue where g1 can be higher than med
			//%dg1 = abs(g1 - med);
			float dg1 = abs(g1 - med);
			//%dg2 = abs(g2 - med);
			float dg2 = abs(g2 - med);
			//%         figure; plot(vec); hold on; plot(peaksLocs, vec(peaksLocs), 'or');
			//%if (dg1 > dg2 && (dg1 / med > (1 - prcThr)...%0.25
			//|| fakeEdgePeak(vec, peaksLocs, peaksLocs(1), estPitch)))

			////test isSym on good peak
			//for (int i = 0; i < peaksLocs.size(); ++i)
			//{
			//	if (isSymPeak(vec, peaksLocs, i))
			//		cout << "peak #" << i << " symmetric" << endl;
			//	else
			//	{
			//		cout << "peak #" << i << " not symmetric" << endl;
			//		Mat lineGraph = plotRBLineWithCenters(vec, peaksLocs);
			//	}
			//}
			//dg1 > dg2
			if (dg1 / med > (1 - prcThr) || !isSymPeak(vec, peaksLocs, 0))
				//if (g1 < g2 && g1 < prcThr * med)
			{
				peaksLocs.erase(peaksLocs.begin());
				nAddLeds--;
			}
			//else if (g2 < 0.5 * med)
			else if (dg2 / med > (1 - prcThr) || !isSymPeak(vec, peaksLocs, peaksLocs.size() - 1))
			{
				peaksLocs.pop_back();
				nAddLeds--;
			}
			else
			{
				cout << "couldnt find peaks" << endl;
				peaksLocs.clear();
				return peaksLocs;
			}

		}

		//missing sanity code from matlab
		/*%% sanity
			dists = diff(peaksLocs);
		medDists = median(dists);
		stdDists = std(dists);
		fact = 1.8;
		if medDists > fact*estPitch || medDists < estPitch / fact || stdDists > estPitch / 2
			peaksLocs = [];
		end*/
		vector<float> dists = vecDiff<float>(peaksLocs);
		float medDists = median(dists);
		float stdDists = calcStd<float>(dists);
		float fact = 1.8;
		if (medDists > fact*estPitch || \
			medDists < estPitch / fact || \
			stdDists > estPitch / 2)
		{
			peaksLocs.clear();
	}

#ifdef DEBUG
		if (debug)
		{
			auto matDebug = plotRBLineWithCenters(vec, peaksLocs);
		}
#endif
		return peaksLocs;
	}

	void getLedPixels(const Point& center, const Size& imSz, const float radius, const bool portait, vector<Point>& upperPix, vector<Point>& lowerPix)
	{		
#ifdef OMP
		int R = ceil(radius);
		for (int x = center.x - R, maxx = center.x+R; x < maxx; x++)
		{
			for (int y = center.y - R, maxy = center.y + R; y < maxy; y++)
#else
		for (int x = 0; x < imSz.width; x++)
		{
			for (int y = 0; y < imSz.height; y++)
#endif
			{
				Point p(x, y);
				Point dxy = p - center;
				float dist = norm(dxy);
				//float dist = norm(p);
				if (dist <= radius)
				{
					if (!portait)
					{
						if (y < center.y)
							upperPix.push_back(p);
						else
							lowerPix.push_back(p);
					}
					else
					{
						if (x >= center.x)
							upperPix.push_back(p);
						else
							lowerPix.push_back(p);
					}
				}
			}
		}
		return;
	}

	//string getLedColor(const Mat& img, vector<Point> led_mask, string optCols, vector<int> hThr)
	string getLedColor(const Mat& img, const Mat& imHsv,vector<Point> led_mask, string optCols, vector<int> hThr)
	{
		Mat giantColMat = img.clone().reshape(0, img.cols * img.rows);
		// Get the hsv version of the bgr frame
		Mat hsv = imHsv.clone().reshape(0, imHsv.cols * imHsv.rows);
		//cvtColor(giantColMat, hsv, COLOR_BGR2HSV);
		vector<Mat> hsvParts;
		//		split(giantColMat, hsvParts);
		split(hsv, hsvParts);
		// Get the gray version of the frame
		Mat grayFrame;
		cvtColor(giantColMat, grayFrame, COLOR_BGR2GRAY);
		int voteR = 0, voteP = 0, voteC = 0;
		for (Point p : led_mask)
		{
			int pixInd = p.y * img.cols + p.x;
			int g = grayFrame.at<uchar>(pixInd, 0);
			if (g > 0.1 * 255 && g < 0.95 * 255)
			{
				int h = hsvParts[0].at<uchar>(pixInd, 0);
//				if (h < 0.4 * 179)
//					h = h + 179;

				if (h>hThr[1] || h<hThr[0])
					voteR++;
				else if (h>hThr[0] && h<hThr[1])
					voteC++;
//				else
//					voteW++;
				//if (h > hThr[2] || h < hThr[0])
				//	voteR++;
				//else if (h > hThr[0] && h < hThr[1])
				//	voteB++;
				//else if (h > hThr[1] && h < hThr[2])
				//	voteP++;
			}
		}
		/*for (int r = 0; r < giantColMat.rows; r++)
		{
			int g = grayFrame.at<uchar>(r, 0);
			if (g > 0.1 * 255 && g < 0.8 * 255)
			{
				int h = hsvParts[0].at<uchar>(r, 0);
				if (h > hThr[2] || h < hThr[0])
					voteR++;
				else if (h > hThr[0] && h < hThr[1])
					voteB++;
				else if (h > hThr[1] && h < hThr[2])
					voteP++;
			}
		}*/

		if (2 * voteR > voteR + voteC)
			return "r";
//		else if (2 * voteP > voteR + voteB + voteP)
//			return "p";
		else if (2 * voteC > voteR + voteC)
			return "c";
		else
			return "u";
	}

	//	string getLedSetCodeByHueThreshold

	string getLedSetCode(vector<Point> centers, int nExpLeds, string optCols, const Mat& im, bool portait, vector<int> hThr)
	{
		std::clock_t start = clock();
		vector<Point> center_difference = vecDiff<Point>(centers);
		vector<float> distance;
		for (auto it = center_difference.begin(); it != center_difference.end(); ++it)
			distance.push_back(norm(*it));
		Scalar_<float> meanPeatch = mean(distance);
		float radius = 0.3 * meanPeatch[0];
		float maxRadius = 0.5 * meanPeatch[0];
		string code(max((size_t)nExpLeds, centers.size()), 'u');
		Mat imHsv;
		cvtColor(im, imHsv, COLOR_BGR2HSV);
		//imshow("imHsv",imHsv);
		//imshow("im",im);

		for (size_t ind = 0; ind < centers.size(); ind++)
		{
			vector<Point> upperPix, lowerPix;
			getLedPixels(centers[ind], im.size(), radius, portait, upperPix, lowerPix);
			// plot upper and lower masks
			/*
			Mat img = im.clone();
			for (auto it = upperPix.begin(); it != upperPix.end(); ++it)
			circle(img, *it, 1, CV_RGB(255, 0, 0), 1);
			for (auto it = lowerPix.begin(); it != lowerPix.end(); ++it)
			circle(img, *it, 1, CV_RGB(0, 255, 0), 1);
			imshow("", img);
			waitKey();
			*/
			// cout << "ind: " << ind << endl;
			//string upperCode = getLedColor(im, upperPix, optCols, hThr);
			string upperCode = getLedColor(im, imHsv, upperPix, optCols, hThr);
			//string upperCode = getLedColor(imHsv, upperPix, optCols, hThr);
			/*
			cout << upperCode << '\t';
			Mat img = im.clone();
			auto cc = upperCode == "r" ? CV_RGB(255, 0, 0) : CV_RGB(0, 0, 255);
			circle(img, centers[ind], 20, cc, 5);
			imshow("image of found leds", img);
			waitKey();
			*/
			//string lowerCode = getLedColor(imHsv, lowerPix, optCols, hThr);
			//string lowerCode = getLedColor(im, lowerPix, optCols, hThr);
			string lowerCode = getLedColor(im, imHsv, lowerPix, optCols, hThr);
			/*
			cout << lowerCode << '\t';
			Mat img = im.clone();
			auto cc = lowerCode == "r" ? CV_RGB(255, 0, 0) : CV_RGB(0, 0, 255);
			circle(img, centers[ind], 20, cc, 5);
			imshow("image of found leds", img);
			waitKey();
			*/

			if (upperCode == lowerCode)
				code[ind] = upperCode[0];
			else if (upperCode == "u")
				code[ind] = lowerCode[0];
			else if (lowerCode == "u")
				code[ind] = upperCode[0];
			else
			{
				string combCodes = upperCode + lowerCode;
				if (combCodes == "cr") code[ind] = '1';
				if (combCodes == "rc") code[ind] = '2';
//				if (combCodes == "bp") code[ind] = '3';
//				if (combCodes == "rp") code[ind] = '4';
//				if (combCodes == "pb") code[ind] = '5';
//				if (combCodes == "pr") code[ind] = '6';
			}
		}
		//std::reverse(code.begin(), code.end());
#ifdef PROFILE
		cout << "Time to get set code: " << clock() - start << endl;
#endif //PROFILE

		return code;
	}


	Mat extractValidMask(Mat &mask, vector<Point> cutXY, double estPeatch)
	{
		Mat validMask = Mat::zeros(mask.size(), mask.type());
		for (Point p : cutXY)
		{
			int pixInd = p.y * mask.cols + p.x;
			int i = p.x;
			int j = p.y;
			int g = (int)mask.at<uchar>(j, i);
			if (g)
			{
				validMask.at<uchar>(j, i) = 255;
			}
		}
		dilate(validMask, validMask, getStructuringElement(CV_SHAPE_ELLIPSE, Size(estPeatch / 2, estPeatch / 2)));
		return validMask;
	}

	//TODO - this function is bad. need to pass image, and use greysacle values to find max intensity!
	bool refineLineParams(const Mat& im,const Mat& mask, const int nExpLeds, float &slope, float &offset, const Mat& ValidMask, const float& estPitch, const float& croppedOffset)
	{
		Mat labels, stats, centroids;
		//matlab: rp = regioprops(mask, 'area', 'Centroid');
		int nLabels = connectedComponentsWithStats(mask, labels, stats, centroids, 8); //Omri - changed connectivity to 8, as in Matlab!

//Omri - fix!
		Mat grayFrame;
		cvtColor(im, grayFrame, COLOR_BGR2GRAY);

		vector<double> area = vector<double>(nLabels);
		vector<double> rowRange = vector<double>(nLabels);	//FOR BOUNDINGBOX
		vector<double> colRange = vector<double>(nLabels);
		vector<double> left = vector<double>(nLabels);
		vector<double> top = vector<double>(nLabels);
		vector<Point> Centers;
		for (int i = 1; i < nLabels; i++)
		{
			area[i] = stats.at<int>(i, CC_STAT_AREA);
			colRange[i] = stats.at<int>(i, CC_STAT_WIDTH);
			rowRange[i] = stats.at<int>(i, CC_STAT_HEIGHT);
			left[i] = stats.at<int>(i, CC_STAT_LEFT);
			top[i] = stats.at<int>(i, CC_STAT_TOP);

			Rect rect(left[i], top[i], colRange[i], rowRange[i]);
			Mat imgBlobBB;
			Point minLoc, maxLoc;

			grayFrame(rect).copyTo(imgBlobBB);

			minMaxLoc(imgBlobBB, NULL, NULL, NULL, &maxLoc);

			double x = maxLoc.x + left[i];
			double y = maxLoc.y + top[i];

			Centers.push_back(Point(x, y));
		}

		/*for(Point p : Centers)
		{
			cout<<"inside for"<<endl;
			int pixInd = p.y * mask.cols + p.x;

				int g = ValidMask.at<uchar>(Point(p.x,p.y));
				cout<<g<<endl;
			if(ValidMask.at<uchar>(Point(p.x,p.y))!=0)
			{
				centers.push_back(Point(p.x,p.y));
			}

		}*/

//TODO - They dont use ValidMask - so they use top 18 centers? should this be 18 or 27? or 9?
		//Not using valid mask create bug in BR/20160530_104826, on second frame

		//matlab: sortedArea = sort(area, 'descend');
		std::sort(area.rbegin(), area.rend());
		int max_area = area[1]; //[0]
		int min_area = 0;
		//if (area.size() > 18)
		if (area.size()> nExpLeds*2/3)
			//min_area = area[17];
			min_area = area[(int)(nExpLeds * 2/3 - 1)];
		else
			min_area = area[area.size() - 1];

		//Omri - try to fix "floating" areas too small..
		//min_area = max(min_area, 4);

		//Omri
		//Mat masktemp = grayFrame.clone();
		//Mat masktemp2 = mask.clone();
		//for (int i = 0; i < grayFrame.cols; ++i)
		//{
		//	int x = i;
		//	int  y = int(double(i) * slope + croppedOffset);
		//	masktemp.at<uchar>(y, x) = 255;
		//	masktemp2.at<uchar>(y, x) = 120;
		//}

		vector<Point> centers;
		for (int i = 0; i < nLabels; i++)
		{
			int a = stats.at<int>(i, CC_STAT_AREA);
			if (a >= min_area && a <= max_area)
			{
				double x = centroids.at<double>(i, 0);
				double y = centroids.at<double>(i, 1);
				//				centers.push_back(Point(rowRange[i]
				

				//omri - condition on distance from estimated line
				//take minimum between x and y distances
				double yDist = abs(y - (croppedOffset + slope * x));
				double xDist = abs(x - (y - croppedOffset) / slope);
				double minDist = min(xDist,yDist);
				if (minDist < estPitch / 2 )
				{
					centers.push_back(Point(x, y));
				}
			}
		}
		int numCenters = centers.size();
		if (numCenters < 3)
		{ 
			return false;
		}

		Vec4f l;  // (vx, vy, x0, y0)
		Mat Ncentroids(centers); //Omri fix - used Centers insteead of centers...
		fitLine(Ncentroids, l, CV_DIST_L2, 0, 0.01, 0.01);
//#ifdef DEBUG
		if (debug)
		{
			Mat img = im.clone();
			for (auto c : centers)
				circle(img, c, 1, CV_RGB(255, 0, 0), 1);
			float t00 = -l[2] / l[0];
			float t1 = (img.cols - 1 - l[2]) / l[0];
			Point l0(0, l[3] + t00 * l[1]);
			Point l1(img.cols - 1, l[3] + t1 * l[1]);
			line(img, l0, l1, CV_RGB(255, 255, 255), 1);

			line(img, Point(l[2]+50*l[0],l[3] + 50 * l[1]), Point(l[2]-50*l[0], l[3] - 50 * l[1]), CV_RGB(120, 120, 120), 1);

			imshow("refineLineParams centers and line", img);
			//waitKey();
		}
//#endif // DEBUG
		

		slope = l[1] / l[0];
		float t0 = -l[2] / l[0]; // (x = 0, y = offset)
		offset = l[3] + t0 * l[1];

		return true;
	}

	void calcBegAndEnd(const Point& maskBegPoint, const vector<Point>& centersLoc, const float pitch, const Size& imSz, Point& begPoint, Point& endPoint)
	{
		int min_center_x = numeric_limits<int>::max();
		int min_center_y = numeric_limits<int>::max();
		int max_center_x = numeric_limits<int>::min();
		int max_center_y = numeric_limits<int>::min();
		for (auto p : centersLoc)
		{
			min_center_x = min(min_center_x, p.x);
			min_center_y = min(min_center_y, p.y);
			max_center_x = max(max_center_x, p.x);
			max_center_y = max(max_center_y, p.y);
		}
		begPoint.x = max(0, (int)round(maskBegPoint.x + min_center_x - pitch));
		begPoint.y = max(0, (int)round(maskBegPoint.y + min_center_y - pitch));
		endPoint.x = min(imSz.width, (int)round(maskBegPoint.x + max_center_x + pitch));
		endPoint.y = min(imSz.height, (int)round(maskBegPoint.y + max_center_y + pitch));
	}

	Point getLedInfo(Mat mask, int &radius)
	{
		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;
		findContours(mask, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		RotatedRect ellipse;
		int maxMaxRadius = 0;
		for (int i = 0; i < contours.size(); i++)
		{
			RotatedRect curEllipse = fitEllipse(contours[i]);
			int curMaxRadius = max(curEllipse.size.width, curEllipse.size.height);
			if (curMaxRadius > maxMaxRadius)
			{
				maxMaxRadius = curMaxRadius;
				ellipse = curEllipse;
			}
		}

		if (maxMaxRadius == 0)
			throw new AlgorithmError("max radius cannot be 0");

		radius = maxMaxRadius;
		return ellipse.center;
	}

	string detectLed(Mat temporalMask, Mat frame, string optCols, bool portrait, vector<int> hThr)
	{
		// Find the center and radius of the LED
		int radius = 0;
		Point center = getLedInfo(temporalMask, radius);
#ifdef DEBUG	
		if (debug)
		{
			Mat dispCpy = frame.clone();
			circle(dispCpy, center, radius, Scalar::all(125));
			imshow("detectLedsNoAoiNoTemplate: single led", dispCpy);
			//waitKey();
		}
#endif
		// extract leds colors
		vector<Point> centersLoc;
		centersLoc.push_back(center);
		string code = getLedSetCode(centersLoc, 1, optCols, frame, portrait, hThr);

		return code;
	}

	//% estimateMissingCenters - addes missing centers to
	//% centers vector, according to mean distance between found leds
	vector<float> estimateMissingCenters(vector<float> centers, int nExpLeds, const float estPitch, vector<float> prevCenters)
	{
		/*
			newCenters = centers;
		if (length(centers) == nExpected)
		return;
		end

		if (isempty(centers))
		return;
		end

		nMissingCenters = nExpected - length(centers);
		pitches = diff(centers);


		*/
		//TODO - there are cases where centers detected are too close (and not too far).
		// Perhaps add code to remove those bad close leds - and then try to fix the rest
		//e.g. check for distance < 0.5*estPitch. if found such - clear the point that has neighbor on other side with lower distance.

		vector<float> newCenters = centers;

		int nMissingCenters = nExpLeds - centers.size();

		vector<float> pitches = vecDiff<float>(centers);
		vector<int> missingIdx;

		/*
			if (find(pitches>=3.5*estPitch))
			disp('Cannot handle more than 2 consecutive missing centers...');
			return;
			end
		*/
		auto iter = find_if(pitches.begin(), pitches.end(), [estPitch](float f) -> bool { return f >= (float)3.5*estPitch; });
		if (iter != pitches.end())
		{
			throw AlgorithmError("Cannot handle more than 2 consecutive missing centers...");
		}


		//	missingIdxBinary = pitches > 1.5*estPitch;
		//	missingIdx = find(missingIdxBinary);
		for (int i = 0; i < pitches.size(); i++)
		{
			if (pitches.at(i) >= 1.5*estPitch)
			{
				missingIdx.push_back(i);
			}
		}

		if (missingIdx.size() > nMissingCenters)
		{
			throw AlgorithmError("Cannot have more missing centers by heuristic than actual missing centers");
		}



		//%		numAdded = 0;		
		int numAdded = 0;
		int updatedIdx = 0;
		vector<float> newAvgCenter;

		//%		for i=1:length(missingIdx)

		for (int i = 0; i < missingIdx.size(); i++)
		{
			//	% one missing led
			//currMissingIdx = missingIdx(i);
			//updatedIdx = currMissingIdx + numAdded;
			int currMissingIdx = missingIdx.at(i);
			updatedIdx = currMissingIdx + numAdded;

			int j = missingIdx.at(i);

			//if (pitches(missingIdx(i)) < 2.5*estPitch)
			if (pitches.at(j) < 2.5*estPitch)
			{
				//	newAvgCenter = round(mean(newCenters(updatedIdx:updatedIdx + 1)));
				float newAvgCenter = round((newCenters[updatedIdx] + newCenters[updatedIdx + 1]) / 2);

				//newCenters = [newCenters(1:updatedIdx); newAvgCenter; newCenters(updatedIdx + 1:end)];
				newCenters.insert(newCenters.begin() + updatedIdx + 1, newAvgCenter);

				//numAdded = numAdded + 1;
				numAdded = numAdded + 1;
			}
			//elseif(pitches(missingIdx(i)) > 2.5*estPitch && pitches(missingIdx(i)) < 3.5*estPitch)
			else if (pitches[j] > 2.5*estPitch && pitches[j] < 3.5*estPitch)
			{
				//	avgPitch = round(diff(newCenters(updatedIdx:updatedIdx + 1)) / 3);
				float avgPitch = round((newCenters[updatedIdx + 1] - newCenters[updatedIdx]) / 3);
				//newCenters = [newCenters(1:updatedIdx); ...
				//	newCenters(updatedIdx) + avgPitch;
				//newCenters(updatedIdx) + 2 * avgPitch;
				//newCenters(updatedIdx + 1:end)];
				newCenters.insert(newCenters.begin() + updatedIdx + 1, newCenters[updatedIdx] + avgPitch);
				newCenters.insert(newCenters.begin() + updatedIdx + 2, newCenters[updatedIdx] + 2 * avgPitch);
				//numAdded = numAdded + 2;
				numAdded = numAdded + 2;
				//end
				//	end
			}
		}


		//	% We found less problematic pitches than should be -
			//% data contains missing led at endpoint
			//if (numAdded < nMissingCenters)
		if (numAdded < nMissingCenters && nMissingCenters - numAdded <= 2 && newCenters.size() > 1)
		{
			//disp('Start or end led missing. Trying to recover with previous frame data');
			//%Compare end centers to previouEnd centers - see who is bigger....
			//if (length(prevFrameCenters) == nExpected)
			if (prevCenters.size() == nExpLeds)
			{
				//firstLedDiff = abs(prevFrameCenters(1) - newCenters(1));
				float firstLedDiff = abs(prevCenters[0] - newCenters[0]);
				//lastLedDiff = abs(prevFrameCenters(end) - newCenters(end));
				float lastLedDiff = abs(prevCenters[nExpLeds - 1] - newCenters[newCenters.size() - 1]);
				//if (firstLedDiff>lastLedDiff)
				if (firstLedDiff > lastLedDiff)
				{
					//newCenters = [newCenters(1)-(newCenters(2)-newCenters(1)) ; newCenters];
					newCenters.insert(newCenters.begin(), std::max(float(0), newCenters[0] - (newCenters[1] - newCenters[0])));
				}
				//else
				else
				{
					//newCenters = [newCenters; newCenters(end)+(newCenters(end)-newCenters(end-1))];
					newCenters.insert(newCenters.end(), newCenters[newCenters.size() - 1] + (newCenters[newCenters.size() - 1] - newCenters[newCenters.size() - 2]));
				}
				//end
			//else
			}
			else
			{
				//disp('Previous frame doesnt have enough leds - cannot conclude missing end led location');
			}
			//end

			//end
		}

		//if (length(newCenters) > nExpected)
		if (newCenters.size() > nExpLeds)
		{
			//disp('Added too much centers - revert to original centers');
			newCenters = centers;
			//% newCenters = uint8(newCenters);
			//end
		}

		//cout << "newcenters:";
		//std::copy(newCenters.begin(), newCenters.end(), std::ostream_iterator<float>(std::cout, " "));
		//cout << endl;

		return newCenters;

	}

	bool processTemporalMask(bool& portrait, bool& rot, float& slope, float& offset, float& estAngle, Mat &mask)
	{
		//imshow("mask", mask);
		//waitKey(0);
		// detect leds by temporal changes
		//Mat mask = detectLedsTemporal(framesArr);  // only here we use all frames
		//Mat mask = Copmask.clone();
#ifdef DEBUG
		if (debug)
		{
			imshow("detectLedsNoAoiNoTemplate: mask", mask * 255);
			//waitKey();
		}
#endif
		if (sum(mask)[0] < maskThr * 255)
		{
			//throw AlgorithmError("sum(mask)[0] < maskThr * 255");
			return false;
		}

		//clean mask
		Mat clmask = mask.clone(); // cleanMask(mask);
#ifdef DEBUG
		if (debug)
		{
			imshow("detectLedsNoAoiNoTemplate: clmask", clmask);
			//waitKey();
		}
#endif
		//crop the mask
		Mat cMask;
		Point beRow, beCol;
		cropMask(clmask, cMask, beRow, beCol);
#ifdef DEBUG
		if (debug)
		{
			imshow("detectLedsNoAoiNoTemplate: cMask", cMask);
			//waitKey();
		}
#endif
		// detect line(angle & location) of leds
		Point imCenterTrans(beCol.x, beRow.x);
		
		detectLedsLineByRadon(cMask, imCenterTrans, slope, offset, estAngle);
		//if (abs(sin(estAngle * CV_PI / 180.0)) < 0.5)
		if (abs(estAngle) < 5 || abs(180 - estAngle) < 5)
		{
			Mat tempMat;
			rot = true;
			//rotate_image_90(frame);
			rotate_image_90(mask);
			rotate_image_90(clmask);
			cropMask(clmask, cMask, beRow, beCol);
			imCenterTrans.x = beCol.x;
			imCenterTrans.y = beRow.x;
			// swap(imCenterTrans.x, imCenterTrans.y);
			portrait = !portrait;
			detectLedsLineByRadon(cMask, imCenterTrans, slope, offset, estAngle);
		}
		else
		{
			rot = false;
		}

		return true;
	}
	
	string detectLedsNoAoiNoTemplate(vector<Mat>& framesArr, int nExpLeds, string optCols, int frame_idx, StateData& stateData, Mat& mask)
	{
		Mat frame = framesArr[frame_idx];  // frame to find leds

		if (stateData.rot)
		{
			rotate_image_90(frame);
		}

		// estimate pitch
		vector<Point> cutXY;
		Mat croppedIm;
		estimatePitchByFFT(frame, mask, stateData.fullFrameSlope, stateData.fullFrameOffset, stateData.estPitch, cutXY);
		if (stateData.estPitch < EPSILON)
		{
			throw AlgorithmError("Estimated pitch in image without ROI is 0");
		}
		// TODO:
		// we could not estimate the pitch, probably because the radon line is not accurate->estimate by closest blobs
		// [estPitch, ~, ~, cutX, cutY] = estPitchByClosestNeighbors(im, slope, offset, mask);
		cutMaskByPitch(frame, stateData.estPitch, cutXY, croppedIm, stateData.begPoint, stateData.endPoint);
#ifdef DEBUG
		if (debug)
		{
			imshow("croppedIm", croppedIm);
			//waitKey();
		}
#endif
		//cut mask by pitch value
		//Rect rect(begPoint.x, begPoint.y, endPoint.x - begPoint.x, endPoint.y - begPoint.y);
		Rect rect(stateData.begPoint.x, stateData.begPoint.y, min(stateData.endPoint.x - stateData.begPoint.x, mask.cols), \
			     min(stateData.endPoint.y - stateData.begPoint.y, mask.rows)); //Omri
																									 //Possible fix...
																															 //Rect rect(begPoint.x, begPoint.y, min(endPoint.x - begPoint.x, mask.cols), min(endPoint.y - begPoint.y, mask.rows)); //Omri
		Mat croppedMask = mask(rect).clone();

		Mat ValidMask;// = extractValidMask(mask,cutXY,estPitch);
					  //ValidMask = ValidMask(rect);

					  //omri - try cropped offset
		stateData.offset = stateData.fullFrameOffset;
		stateData.slope = stateData.fullFrameSlope;
		float croppedOffset = stateData.offset + stateData.begPoint.x*stateData.slope;
		croppedOffset = croppedOffset - rect.y;

		
		if (!refineLineParams(croppedIm, croppedMask, nExpLeds, stateData.slope, stateData.offset, ValidMask, stateData.estPitch, croppedOffset))
			stateData.offset = croppedOffset;
		estimatePitchByFFT(croppedIm, croppedMask, stateData.slope, stateData.offset, stateData.estPitch, cutXY);
		if (stateData.estPitch < EPSILON)
		{
			throw AlgorithmError("Estimated pitch in image without ROI, after refinement, is 0");
		}
		//locate centers	
		vector<float> rbLine = extract_rb_Line(croppedIm, stateData.slope, stateData.offset);
		vector<int> x, y;
		extractXYCoords(croppedIm, stateData.slope, stateData.offset, x, y);
#ifdef DEBUG
		if (debug && true)
		{
			int range[2] = { 0, 300 };
			Mat lineGraph = plotGraph(rbLine, range);
			cv::imshow("rbLine", lineGraph);
			//waitKey();
		}
#endif

		vector<float> centers = locatePeaks(rbLine, stateData.estPitch, nExpLeds);
		if (centers.empty())
			throw AlgorithmError("peaks not found 1");


		if (centers.size() < nExpLeds)
		{
			try
			{
				/*cout << "prevCenters           :";
				std::copy(prevCenters.begin(), prevCenters.end(), std::ostream_iterator<float>(std::cout, " "));
				cout << endl;
				cout << "before missing centers:";
				std::copy(centers.begin(), centers.end(), std::ostream_iterator<float>(std::cout, " "));
				cout << endl;*/
				centers = estimateMissingCenters(centers, nExpLeds, stateData.estPitch, prevCenters);
				/*cout << "after  missing centers:";
				std::copy(centers.begin(), centers.end(), std::ostream_iterator<float>(std::cout, " "));
				cout << endl;*/

			}
			catch (const AlgorithmError& e)
			{
#ifdef DEBUG
				if (debug)
					cout << "Error in estimating missing centers: " << e.what() << endl;
#endif
			}
		}

		if (centers.size() != nExpLeds)
			throw AlgorithmError("Failed to found led centers - No AOI");
		prevCenters = centers;


		// centersLocX = x(centers);
		// centersLocY = y(centers);
		// here line is always horizontal 
		//Omri - after my latest fixes, this is not the case. What if we output the x and y with the cutXY like in matlab?
		vector<Point> centersLoc;
		for (auto c : centers)
		{
			//float y = round(offset + slope * c);
			centersLoc.push_back(Point(x[c], y[c]));
		}
		//#ifdef DEBUG
		//if (debug && true)
		{
			// plot found leds
			Mat img = croppedIm.clone();
			for (auto c : centersLoc)
				circle(img, c, 5, CV_RGB(255, 0, 0), 1);
#ifdef DEBUG
			if (debug && true)
			{
				imshow("image of found leds", img);
				//waitKey();
			}
#endif
		}
		//#endif
		// where should the image be cropped ? (for next frames)
		//TODO - Omri - passing begPoint twice as reference (first const, second non-const), and changing it in the funciton is wrong!
		//because next frame has bad cropping
		Point oldBegPoint = stateData.begPoint;
		calcBegAndEnd(oldBegPoint, centersLoc, stateData.estPitch, frame.size(), stateData.begPoint, stateData.endPoint);

		/*	if (rot)
		{
		int tmp = begPoint.x;
		begPoint.x = begPoint.y;
		begPoint.y = tmp;

		tmp = endPoint.x;
		endPoint.x = endPoint.y;
		endPoint.y = tmp;
		}*/

		// extract leds colors
		string code = getLedSetCode(centersLoc, nExpLeds, optCols, croppedIm, stateData.portrait, stateData.hThr);
		// plot found leds with color
		/**/
#ifdef DEBUG
		if (debug)
		{
			Mat img = croppedIm.clone();
			for (int i = 0; i < min(code.size(), centersLoc.size()); i++)
			{
				Scalar c;
				switch (code[i])
				{
				case 'r':
					c = CV_RGB(255, 0, 0);
					break;
				case 'c':
					c = CV_RGB(0, 255, 255);
					break;
				case 'p':
					c = CV_RGB(196, 0, 171);
					break;
				case 'g':
					c = CV_RGB(0, 255, 0);
					break;
				default:
					c = CV_RGB(255, 255, 255);
					break;
				}

				circle(img, centersLoc[i], 10, c, 3);
			}
			imshow("detectLedsNoAoiNoTemplate: image of found leds", img);
			//waitKey();
		}
#endif  // DEBUG
		/**/
		return code;

	}


	string detectLedsNoAoiNoTemplate(vector<Mat>& framesArr, int nExpLeds, string optCols, int frame_idx, bool portrait, Point& begPoint, Point& endPoint, float& estPitch, bool& rot, vector<int> hThr, Mat &mask)
	{
		//imshow("mask", mask);
		//waitKey(0);
		// detect leds by temporal changes
		//Mat mask = detectLedsTemporal(framesArr);  // only here we use all frames
		//Mat mask = Copmask.clone();
#ifdef DEBUG
		if (debug)
		{
			imshow("detectLedsNoAoiNoTemplate: mask", mask * 255);
			//waitKey();
		}
#endif
		if (sum(mask)[0] < maskThr * 255)
			throw AlgorithmError("sum(mask)[0] < maskThr * 255");
		Mat frame = framesArr[frame_idx];  // frame to find leds
#ifdef DEBUG
		if (debug)
		{
			imshow("detectLedsNoAoiNoTemplate: frame", frame);
			//waitKey();
		}
#endif

		//clean mask
		Mat clmask = mask.clone(); // cleanMask(mask);
#ifdef DEBUG
		if (debug)
		{
			imshow("detectLedsNoAoiNoTemplate: clmask", clmask);
			//waitKey();
		}
#endif
		//crop the mask
		Mat cMask;
		Point beRow, beCol;
		cropMask(clmask, cMask, beRow, beCol);
#ifdef DEBUG
		if (debug)
		{
			imshow("detectLedsNoAoiNoTemplate: cMask", cMask);
			//waitKey();
		}
#endif
		// detect line(angle & location) of leds
		Point imCenterTrans(beCol.x, beRow.x);
		float slope, offset, estAngle;
		detectLedsLineByRadon(cMask, imCenterTrans, slope, offset, estAngle);
		//if (abs(sin(estAngle * CV_PI / 180.0)) < 0.5)
		if (abs(estAngle) < 20 || abs(180-estAngle) < 20)
		{
			Mat tempMat;
			rot = true;
			rotate_image_90(frame);
			rotate_image_90(mask);
			rotate_image_90(clmask);
			cropMask(clmask, cMask, beRow, beCol);
			imCenterTrans.x = beCol.x;
			imCenterTrans.y = beRow.x;
			// swap(imCenterTrans.x, imCenterTrans.y);
			portrait = !portrait;
			detectLedsLineByRadon(cMask, imCenterTrans, slope, offset, estAngle);
		}
		else
			rot = false;
		// estimate pitch
		vector<Point> cutXY;
		Mat croppedIm;
		estimatePitchByFFT(frame, mask, slope, offset, estPitch, cutXY);
		if (estPitch < EPSILON)
		{
			throw AlgorithmError("Estimated pitch in image without ROI is 0");
		}
		// TODO:
		// we could not estimate the pitch, probably because the radon line is not accurate->estimate by closest blobs
		// [estPitch, ~, ~, cutX, cutY] = estPitchByClosestNeighbors(im, slope, offset, mask);
		cutMaskByPitch(frame, estPitch, cutXY, croppedIm, begPoint, endPoint);
#ifdef DEBUG
		if (debug)
		{
			imshow("croppedIm", croppedIm);
			//waitKey();
		}
#endif
		//cut mask by pitch value
		//Rect rect(begPoint.x, begPoint.y, endPoint.x - begPoint.x, endPoint.y - begPoint.y);
		Rect rect(begPoint.x, begPoint.y, min(endPoint.x - begPoint.x, mask.cols), min(endPoint.y - begPoint.y, mask.rows)); //Omri
		
		//Possible fix...
//Rect rect(begPoint.x, begPoint.y, min(endPoint.x - begPoint.x, mask.cols), min(endPoint.y - begPoint.y, mask.rows)); //Omri
		Mat croppedMask = mask(rect).clone();

		Mat ValidMask;// = extractValidMask(mask,cutXY,estPitch);
		//ValidMask = ValidMask(rect);

		//omri - try cropped offset
		float croppedOffset = offset + begPoint.x*slope;
		croppedOffset = croppedOffset - rect.y;

		if (!refineLineParams(croppedIm, croppedMask, nExpLeds, slope, offset, ValidMask, estPitch, croppedOffset))
			offset = croppedOffset;
		estimatePitchByFFT(croppedIm, croppedMask, slope, offset, estPitch, cutXY);
		if (estPitch < EPSILON)
		{
			throw AlgorithmError("Estimated pitch in image without ROI, after refinement, is 0");
		}
		//locate centers	
		vector<float> rbLine = extract_rb_Line(croppedIm, slope, offset);
		vector<int> x, y;
		extractXYCoords(croppedIm, slope, offset,x,y);
#ifdef DEBUG
		if (debug && true)
		{
			int range[2] = { 0, 300 };
			Mat lineGraph = plotGraph(rbLine, range);
			cv::imshow("rbLine", lineGraph);
			//waitKey();
		}
#endif

		vector<float> centers = locatePeaks(rbLine, estPitch, nExpLeds);
		if (centers.empty())
			throw AlgorithmError("peaks not found 1");


		if (centers.size() < nExpLeds)
		{
			try
			{
				/*cout << "prevCenters           :";
				std::copy(prevCenters.begin(), prevCenters.end(), std::ostream_iterator<float>(std::cout, " "));
				cout << endl;
				cout << "before missing centers:";
				std::copy(centers.begin(), centers.end(), std::ostream_iterator<float>(std::cout, " "));
				cout << endl;*/
				centers = estimateMissingCenters(centers, nExpLeds, estPitch, prevCenters);
				/*cout << "after  missing centers:";
				std::copy(centers.begin(), centers.end(), std::ostream_iterator<float>(std::cout, " "));
				cout << endl;*/

			}
			catch (const AlgorithmError& e)
			{
#ifdef DEBUG
				if (debug)
					cout << "Error in estimating missing centers: " << e.what()<< endl;
#endif
			}
		}

		if (centers.size() != nExpLeds)
			throw AlgorithmError("Failed to found led centers - No AOI");
		prevCenters = centers;


		// centersLocX = x(centers);
		// centersLocY = y(centers);
		// here line is always horizontal 
		//Omri - after my latest fixes, this is not the case. What if we output the x and y with the cutXY like in matlab?
		vector<Point> centersLoc;
		for (auto c : centers)
		{
			//float y = round(offset + slope * c);
			centersLoc.push_back(Point(x[c], y[c]));
		}
//#ifdef DEBUG
		//if (debug && true)
		{
			// plot found leds
			Mat img = croppedIm.clone();
			for (auto c : centersLoc)
				circle(img, c, 5, CV_RGB(255, 0, 0), 1);
#ifdef DEBUG
			if (debug && true)
			{
				imshow("image of found leds", img);
				//waitKey();
			}
#endif
		}
//#endif
		// where should the image be cropped ? (for next frames)
		//TODO - Omri - passing begPoint twice as reference (first const, second non-const), and changing it in the funciton is wrong!
		//because next frame has bad cropping
		Point oldBegPoint = begPoint;
		calcBegAndEnd(oldBegPoint, centersLoc, estPitch, frame.size(), begPoint, endPoint);

	/*	if (rot)
		{
			int tmp = begPoint.x;
			begPoint.x = begPoint.y;
			begPoint.y = tmp;

			tmp = endPoint.x;
			endPoint.x = endPoint.y;
			endPoint.y = tmp;
		}*/

		// extract leds colors
		string code = getLedSetCode(centersLoc, nExpLeds, optCols, croppedIm, portrait, hThr);
		// plot found leds with color
		/**/
#ifdef DEBUG
		if (debug)
		{
			Mat img = croppedIm.clone();
			for (int i = 0; i < min(code.size(), centersLoc.size()); i++)
			{
				Scalar c;
				switch (code[i])
				{
				case 'r':
					c = CV_RGB(255, 0, 0);
					break;
				case 'c':
					c = CV_RGB(0, 255, 255);
					break;
				case 'p':
					c = CV_RGB(196, 0, 171);
					break;
				case 'g':
					c = CV_RGB(0, 255, 0);
					break;
				default:
					c = CV_RGB(255, 255, 255);
					break;
				}

				circle(img, centersLoc[i], 10, c, 3);
			}
			imshow("detectLedsNoAoiNoTemplate: image of found leds", img);
			//waitKey();
		}
#endif  // DEBUG
		/**/
		return code;
	}

	vector<int> extractMaskIdx(const Mat& mask)
	{
		vector<int> res;
		auto p = mask.ptr<uint8_t>(0);
		for (int i = 0; i < mask.rows*mask.cols; ++i)
		{
			if (p[i])
				res.push_back(i);
		}
		return res;
	}

	void extractXYCoords(const Mat& im, const float slope, const float offset, vector<int>& xv, vector<int>& yv)
	{
		if (abs(slope) < 0.5) {
			for (int x = 0; x < im.cols; x++) {
				int y = int(offset + slope * x);
				if (y >= 0 && y < im.rows)
				{
					yv.push_back(y);
					xv.push_back(x);
				}
			}
		}
		else {
			for (int y = 0; y < im.rows; y++) {
				int x = int(((double)y - offset) / slope);
				if (x >= 0 && x < im.cols)
				{
					xv.push_back(x);
					yv.push_back(y);
				}
			}
		}
	}

	vector<int> calculateHueBasedThreshold_fast(const vector<Mat>& framesArr, Mat &mask)
	{

		//Mat mask = detectLedsTemporal(framesArr);

		//Mat Cmask = mask.clone().reshape(0, 1);
		Mat Cmask = mask.clone().reshape(1, 1);
		vector<int> maskIdx = extractMaskIdx(Cmask);

        int sz[3] = { static_cast<int>(maskIdx.size()*framesArr.size()),1 };

		//Mat giantColMat(3, sz, CV_8U,0);
		Mat giantColMat(maskIdx.size()*framesArr.size(), 1, CV_8UC3, Scalar(0, 0, 0));
		//auto giantColtMatP = giantColMat.ptr<CV_8U>(0);
		auto giantColtMatP = giantColMat.ptr<uint8_t>(0);

		int counter = 0;

		for (auto frame : framesArr) {
			if (frame.rows == 0 || frame.cols == 0) continue;
			Mat rowMat = frame.clone().reshape(0, 1);
			/*for (int i = 0; i < rowMat.cols; i++)
			if (Cmask.at<uchar>(0, i) != 0)
			giantColMat.push_back(rowMat.at<Vec3b>(i));*/

			//new code
			for (int i = 0; i < maskIdx.size(); ++i)
			{
				//*(giantColtMatP + counter) = rowMat.at<Vec3b>(maskIdx[i]);
				*(giantColtMatP + counter++) = *(rowMat.data + maskIdx[i] * 3);
				*(giantColtMatP + counter++) = *(rowMat.data + maskIdx[i] * 3 + 1);
				*(giantColtMatP + counter++) = *(rowMat.data + maskIdx[i] * 3 + 2);
			}


			/*if (giantColMat.empty())
			giantColMat = rowMat;
			else
			hconcat(giantColMat, rowMat);*/
		}
		//		giantColMat.t();
		// Get the hsv version of the bgr frame
		Mat hsv;
		cvtColor(giantColMat, hsv, COLOR_BGR2HSV);
		vector<Mat> hsvParts;
		split(hsv, hsvParts);
#ifdef DEBUG
		imshow("hsv1", hsvParts[1]);
		//waitKey();
#endif  // DEBUG
		// Get the gray version of the frame
		Mat grayFrame;
		cvtColor(giantColMat, grayFrame, COLOR_BGR2GRAY);
		// Only include in the mask pixels within a certain intensity range and value range
		Mat validPixelsHues;
		Mat validPixelsMask = grayFrame < 255 * 0.8 & grayFrame > 255 * 0.1 &
			hsvParts[2] > 0.75 * 255 & hsvParts[2] < 0.99 * 255 &
			hsvParts[0] > 0.4 * 179 & hsvParts[0] < 0.9 * 179;

		for (int r = 0; r < giantColMat.rows; r++)
			if (validPixelsMask.at<uchar>(r, 0) != 0) {
				validPixelsHues.push_back((float)hsvParts[0].at<uchar>(r, 0));
			}
		Mat centers, labels;
		try {
			kmeans(validPixelsHues, 3, labels, TermCriteria(CV_TERMCRIT_ITER, 100, 0.1), 100, KMEANS_PP_CENTERS, centers);
		}
		catch (Exception& e) {
			printf(e.msg.c_str());
		}

		double min, max;
		cv::minMaxLoc(centers, &min, &max);
		vector<int> rpbDivisions;
		rpbDivisions.push_back(0.4 * 179);
		rpbDivisions.push_back((mean(centers)[0] + max) / 2);
		rpbDivisions.push_back(0.9 * 179);
		return rpbDivisions;
	}

	void plotHist(const Mat& hues)
	{
		//int histSize = 100;
		//int histSize = 256;

		int histSize[] = { 100 };
		// hue varies from 0 to 179, see cvtColor
		float hranges[] = { 0, 180+71 };
		const float* ranges[] = { hranges};
		Mat hist;
		// we compute the histogram from the 0-th and 1-st channels
		int channels[] = { 0 };


		double maxVal = 0;
		bool uniform = true; bool accumulate = false;

		//calcHist(&hues, 1, 0, Mat(), hist, 1, &histSize, &ranges, uniform, accumulate);
		calcHist(&hues, 1, channels, Mat(), hist, 1, histSize, ranges);

		minMaxLoc(hist, 0, &maxVal, 0, 0);

		int hist_w = 512; int hist_h = 400;
		int bin_w = cvRound((double)hist_w / histSize[0]);
		Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));

		for (int i = 1; i < histSize[0]; i++)
		{
			line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
				Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
				Scalar(255, 0, 0), 2, 8, 0);
		}
		/*	int scale = 10;

											
		Mat histImg = Mat::zeros(hbins*scale, hbins * 10, CV_8UC3);

		for (int h = 0; h < hbins; h++)
			{
				float binVal = hist.at<float>(h, 0);
				int intensity = cvRound(binVal * 255 / maxVal);
				rectangle(histImg, Point(h*scale, s*scale),
					Point((h + 1)*scale - 1, (s + 1)*scale - 1),
					Scalar::all(intensity),
					CV_FILLED);
			}

		namedWindow("Source", 1);
		imshow("Source", src);

		namedWindow("H-S Histogram", 1);
		*/
		imshow("H-S Histogram", histImage);
		//waitKey();
	}

	vector<int> calculateHueBasedThreshold(const vector<Mat>& framesArr, Mat &mask)
	{
        /*
		Mat giantColMat;
		//Mat mask = detectLedsTemporal(framesArr);

		Mat Cmask = mask.clone().reshape(0, 1);

Mat maskCoordinates;
		findNonZero(Cmask, maskCoordinates);
		for (auto frame : framesArr) {
			if (frame.rows == 0 || frame.cols == 0) continue;
			Mat rowMat = frame.clone().reshape(0, 1);
			for (int i = 0; i < maskCoordinates.total(); i++)
				giantColMat.push_back(rowMat.at<Vec3b>(maskCoordinates.at<Point>(i).x));
//			if (giantColMat.empty())
//				giantColMat = rowMat;
//			else
//				hconcat(giantColMat, rowMat);
		//}
//		giantColMat.t();
		// Get the hsv version of the bgr frame
		Mat hsv;
		cvtColor(giantColMat, hsv, COLOR_BGR2HSV);
		vector<Mat> hsvParts;
		split(hsv, hsvParts);
#ifdef DEBUG
		imshow("hsv1", hsvParts[1]);
		waitKey();
#endif  // DEBUG
		// Get the gray version of the frame
		Mat grayFrame;
		cvtColor(giantColMat, grayFrame, COLOR_BGR2GRAY);
		// Only include in the mask pixels within a certain intensity range and value range
		Mat validPixelsHues;
		
		Mat grayMask = grayFrame < 255 * 0.8 & grayFrame > 255 * 0.1;
		Mat vMask = hsvParts[2] > 0.75 * 255 & hsvParts[2] < 0.99 * 255;
		Mat hMask = hsvParts[0] > 0.4 * 179 & hsvParts[0] < 0.95 * 179;

		Mat validPixelsMask = grayFrame < 255 * 0.8 & grayFrame > 255 * 0.1 &
			hsvParts[2] > 0.75 * 255 & hsvParts[2] < 0.99 * 255 &
			hsvParts[0] > 0.4 * 179 & hsvParts[0] < 0.95 * 179;

		for (int r = 0; r < giantColMat.rows; r++)
			if (validPixelsMask.at<uchar>(r, 0) != 0) {
				validPixelsHues.push_back((float)hsvParts[0].at<uchar>(r, 0));
			}

		//		plotHist(validPixelsHues);

		Mat centers, labels;
		vector<int> rpbDivisions;

		//old code- -using 3 centers for some reason
		////Timer timer(true);
		//try {
		//	kmeans(validPixelsHues, 3, labels, TermCriteria(CV_TERMCRIT_ITER, 100, 0.1), 100, KMEANS_PP_CENTERS, centers);
		//}
		//catch (Exception& e) {
		//	printf(e.msg.c_str());
		//}

		//double min, max;
		//cv::minMaxLoc(centers, &min, &max);
		//
		//rpbDivisions.push_back(0.4 * 179);
		//if (centers.total() > 0)
		//	rpbDivisions.push_back((mean(centers)[0] + max) / 2);
		//else
		//	rpbDivisions.push_back(0.75 * 179);
		//rpbDivisions.push_back(0.9 * 179);

		//std::cout << "rpbDivisions[1]" << rpbDivisions[1] << "kmeans(3): " << std::fixed << timer << "ms\n";

		//timer.Reset();
//Omri
		try {
			kmeans(validPixelsHues, 2, labels, TermCriteria(CV_TERMCRIT_ITER, 100, 0.1), 100, KMEANS_PP_CENTERS, centers);
		}
		catch (Exception& e) {
			printf(e.msg.c_str());
		}
    */
    vector<int> rpbDivisions;

		rpbDivisions.clear();
//		double min, max;
		//cv::minMaxLoc(centers, &min, &max);
		rpbDivisions.push_back(0.3 * 179);
//		if (centers.total() > 0)
//			rpbDivisions.push_back(mean(centers)[0]);
//		else
//			rpbDivisions.push_back(0.75 * 179);
    rpbDivisions.push_back(0.8 * 179); //looking at cyan in hsv - it seems at ~0.6 %mean(means);
		rpbDivisions.push_back(0.95 * 179);

//		std::cout << "rpbDivisions[1]" << rpbDivisions[1] <<"kmeans(2): " << std::fixed << timer << "ms\n";

		return rpbDivisions;
	}

	//dynamic threshold for all colors - based on 3 kmeans centers
	vector<int> calculateHueBasedThresholdDynamicAll(const vector<Mat>& framesArr, Mat &mask)
	{
/*		
Mat giantColMat;
		

		Mat Cmask = mask.clone().reshape(0, 1);

		Mat maskCoordinates;
		findNonZero(Cmask, maskCoordinates);
		for (auto frame : framesArr) {
			if (frame.rows == 0 || frame.cols == 0) continue;
			Mat rowMat = frame.clone().reshape(0, 1);
			for (int i = 0; i < maskCoordinates.total(); i++)
				giantColMat.push_back(rowMat.at<Vec3b>(maskCoordinates.at<Point>(i).x));
		}
		// Get the hsv version of the bgr frame
		Mat hsv;
		cvtColor(giantColMat, hsv, COLOR_BGR2HSV);
		vector<Mat> hsvParts;
		split(hsv, hsvParts);
		// Get the gray version of the frame
		Mat grayFrame;
		cvtColor(giantColMat, grayFrame, COLOR_BGR2GRAY);
		// Only include in the mask pixels within a certain intensity range and value range
		Mat validPixelsHues;
		Mat validPixelsMask = grayFrame < 255 * 0.8 & grayFrame > 255 * 0.1 &
			hsvParts[2] > 0.75 * 255 & hsvParts[2] < 0.99 * 255;


		//TODO - slow iteration - can run a bit faster with pointer arithmetics
		for (int r = 0; r < giantColMat.rows; r++) {
			if (validPixelsMask.at<uchar>(r, 0) != 0) {
				float val = (float)hsvParts[0].at<uchar>(r, 0);
				if (val < 0.4 * 179)
					val = 179 + val; //move "red" values to > 1 - so we can keamns on sequential range [0.4 1.4]
				validPixelsHues.push_back(val);
			}
		}
		//plotHist(validPixelsHues);

		Mat centers, labels;
		vector<int> rpbDivisions;

		//Omri
		try {
			kmeans(validPixelsHues, 3, labels, TermCriteria(CV_TERMCRIT_ITER, 100, 0.1), 100, KMEANS_PP_CENTERS, centers);
		}
		catch (Exception& e) {
			printf(e.msg.c_str());
		}

		std::vector<float> array(centers.rows*centers.cols);
		auto cenP = centers.ptr<float>();
		for (int i = 0; i < 3; ++i)
			array[i] = *cenP++;

		std::sort(array.begin(), array.end());
		//		double min, max;
		//cv::minMaxLoc(centers, &min, &max);
		float bpTh = (array[0] + array[1]) / 2;
		float prTh = (array[1] + array[2]) / 2;
*/

		//rpbDivisions.push_back(bpTh);
		//rpbDivisions.push_back(prTh);

vector<int> rpbDivisions;

//		double min, max;
		//cv::minMaxLoc(centers, &min, &max);
		rpbDivisions.push_back(0.3 * 179);
//		if (centers.total() > 0)
//			rpbDivisions.push_back(mean(centers)[0]);
//		else
//			rpbDivisions.push_back(0.75 * 179);
    //rpbDivisions.push_back(0.8 * 179); //looking at cyan in hsv - it seems at ~0.6 %mean(means);
		rpbDivisions.push_back(0.95 * 179);
		
//        if (centers.total() > 0)
		//            rpbDivisions.push_back(mean(centers)[0]);
		//        else
		//            rpbDivisions.push_back(0.75 * 179);
		//        rpbDivisions.push_back(0.95 * 179);

		//		std::cout << "rpbDivisions[1]" << rpbDivisions[1] <<"kmeans(2): " << std::fixed << timer << "ms\n";

		return rpbDivisions;
	}

	string detedLedsInAoi(const Mat& frame, const int nExpLeds, const string optCols, const bool portrait, Point& begPoint, Point& endPoint, float& pitch, vector<int> hThr)
	{
#ifdef DEBUG
		if (debug)
		{
			imshow("detedLedsInAoi: frame", frame);
			//waitKey();
		}
#endif
		bool cleanMaskFlag = false;

		const int gap = 10;
		int curBegX = max(0, begPoint.x - gap);
		int curBegY = max(0, begPoint.y - gap);
		int curEndX = min(frame.size().width, endPoint.x + gap);
		int curEndY = min(frame.size().height, endPoint.y + gap);

		Rect rect(curBegX, curBegY, curEndX - curBegX, curEndY - curBegY);
		Mat croppedIm = frame(rect).clone();
#ifdef DEBUG
		if (debug)
		{
			imshow("detedLedsInAoi: croppedIm", croppedIm);
			//waitKey();
		}
#endif
		//matlab: colMask = createRMask(croppedIm) | createBMask(croppedIm)
		//create color mask
		Mat Bmask = createCMask(croppedIm);
		Mat Rmask = createRMask(croppedIm);
#ifdef DEBUG
		if (debug)
		{
			imshow("detedLedsInAoi: Bmask", Bmask);
			imshow("detedLedsInAoi: Rmask", Rmask);
			//waitKey();
		}
#endif
		// create colors mask
		Mat colMask;
		bitwise_or(Bmask, Rmask, colMask);

		Mat cMask;
		if (cleanMaskFlag)
		{
			cMask = cleanMask(colMask);
#ifdef DEBUG
			if (debug)
			{
				imshow("detedLedsInAoi: cMask (cleanMask)", cMask);
				//waitKey();
			}
#endif
			double maxBlobSz = (pitch * pitch) / 2;
			cMask = removeBigBlobs(cMask, maxBlobSz);
		}
		else
		{
			cMask = colMask.clone();
		}
			
#ifdef DEBUG
		if (debug & false)
		{
			imshow("detedLedsInAoi: cMask (removeBigBlobs)", cMask);
			//waitKey();
		}
#endif
		int sumCMask = cv::sum(cMask)[0];
		if (cv::sum(cMask)[0] == 0)
			throw AlgorithmError("cv::sum(cMask)[0] cannot be 0");
		// locate the line
		float slope, offset, estPitch;
		Mat ValidMask;// = extractValidMask(cMask,cutXY,pitch);

		//omri - try croppedOffset and add missing RADON??
		float estAngle;
		Point imCenterTrans(0, 0);
		detectLedsLineByRadon(cMask, imCenterTrans, slope, offset, estAngle);

		// estimate pitch - 
		//omri - do we need this?
		vector<Point> cutXY;
		estimatePitchByFFT(croppedIm, cMask, slope, offset, estPitch, cutXY);
		if (estPitch < EPSILON || std::isinf(slope) || std::isinf(-slope))
		{
			throw AlgorithmError("Estimated pitch in image with ROI is 0");
		}
	/*	float croppedOffset = offset + begPoint.x*slope;
		croppedOffset = rect.y + rect.height - croppedOffset;*/
		//omri

		refineLineParams(croppedIm,cMask, nExpLeds, slope, offset, ValidMask, estPitch, offset);
		//locate centers	
		vector<float> rbLine = extract_rb_Line(croppedIm, slope, offset);
		vector<int> x, y;
		extractXYCoords(croppedIm, slope, offset, x, y);
#ifdef DEBUG
		if (debug)
		{
			int range[2] = { 0, 300 };
			Mat lineGraph = plotGraph(rbLine, range);
			cv::imshow("rbLine", lineGraph);
			//waitKey(1);
		}
#endif
		vector<float> centers = locatePeaks(rbLine, estPitch, nExpLeds);
		if (centers.empty())
			throw AlgorithmError("peaks not found2");

		if (centers.size() < nExpLeds)
		{
			try
			{
				/*cout << "prevCenters           :";
				std::copy(prevCenters.begin(), prevCenters.end(), std::ostream_iterator<float>(std::cout, " "));
				cout << endl;
				cout << "before missing centers:";
				std::copy(centers.begin(), centers.end(), std::ostream_iterator<float>(std::cout, " "));
				cout << endl;*/
				centers = estimateMissingCenters(centers, nExpLeds, pitch, prevCenters);
				/*cout << "after  missing centers:";
				std::copy(centers.begin(), centers.end(), std::ostream_iterator<float>(std::cout, " "));
				cout << endl;*/

			}
			catch (const AlgorithmError& e)
			{
#ifdef DEBUG
				if (debug)
					cout << "Error in estimating missing centers: " << e.what() << endl;
#endif
			}
		}

		if (centers.size() != nExpLeds)
			throw AlgorithmError("Failed to found led centers - With AOI");

		prevCenters = centers;

		// calculate x, y
		vector<Point> centersLoc;
		for (auto c : centers)
		{
			//float y = round(offset + slope * c);
			//centersLoc.push_back(Point(int(c), int(y)));

			centersLoc.push_back(Point(x[c], y[c]));
		}

//#ifdef DEBUG
		if (debug & true)
		{
			Mat img = croppedIm.clone();
			for (int i = 0; i < centersLoc.size(); i++)
			{
				circle(img, centersLoc[i], 5, CV_RGB(255, 0, 0), 1);
			}
			imshow("image with found centers", img);
			//waitKey();
		}
//#endif

		// calculate pitch
		sort(centers.begin(), centers.end());
		vector<float> dists = vecDiff(centers);
		pitch = median(dists);
		// where should the image be cropped? (for next frames)
		Point prevBegPoint = Point(curBegX,curBegY);
		calcBegAndEnd(prevBegPoint, centersLoc, pitch, frame.size(), begPoint, endPoint);


		//<MAtlab if (rot) [begX, endX, begY, endY] = calcBegAndEnd(curBegX, curBegY,  centersLocY,centersLocX, pitch, imSz);


		// extract colors
		string code = getLedSetCode(centersLoc, nExpLeds, optCols, croppedIm, portrait, hThr);
		// plot found leds with color
#ifdef DEBUG
		if (debug & true)
		{
			Mat img = croppedIm.clone();
			for (int i = 0; i < min(code.size(), centersLoc.size()); i++)
			{
				Scalar c;
				switch (code[i])
				{
				case 'r':
					c = CV_RGB(255, 0, 0);
					break;
				case 'b':
					c = CV_RGB(0, 0, 255);
					break;
				case 'p':
					c = CV_RGB(196, 0, 171);
					break;
				case 'g':
					c = CV_RGB(0, 255, 0);
					break;
				default:
					c = CV_RGB(255, 255, 255);
					break;
				}

				circle(img, centersLoc[i], 10, c, 3);
			}
			imshow("image of found leds", img);
			//waitKey(10);			
			
		}
#endif  // DEBUG
		return code;
	}

	/* For videos the first argument is the videos path.
	For image sequences it's a string formatted like a printf statement.
	e.g.
	C://sampleVid.avi
	or
	C://sample_image_sequence_folder//frame_image_prefix_%d.jpg
	*/


	vector<string> extractBrightCodeFromVideo(string vidPath, int nExpLeds, float aoiSz, string optCols)
	{
		vector<string> leds_codes;
		VideoCapture FramesStream(vidPath);
		// read the first frames for temporal mask creation
		vector<Mat> framesArr;
		Mat frame;
		Timer timer(true);
		readFirstFrames(FramesStream, temporalMaskFramesNum, aoiSz, framesArr);
		// Calculate the hue thresholds
		std::cout << "readFirstFrames: " << std::fixed << timer << "ms\n";

		timer.Reset();
		Timer detectLedsTemp(true);
		Mat mask = detectLedsTemporal(framesArr);
		std::cout << "detectLedsTemporal: " << std::fixed << detectLedsTemp << "ms\n";
		Timer timer2(true);
		//imwrite("d:\\temporalMAsk.jpg", mask);
		vector<int> hThr = calculateHueBasedThresholdDynamicAll(framesArr, mask);


		//vector<int> hThr = calculateHueBasedThreshold(framesArr, mask); //Aleksey code even faster than my _fast code

		std::cout << "calculateHueBasedThreshold time: " << std::fixed << timer2 << "ms\n";

		// direction of division 
		bool portrait = framesArr[0].size().height > framesArr[0].size().width;
		float estPitch = 0;
		bool rot;
		Point begPoint, endPoint;

		if (nExpLeds == 1) {
			// Create the temporal mask
			Mat temporalMask = detectLedTemporal(framesArr);

			// Detect leds by temporal changes
#ifdef DEBUG
			if (debug)
			{
				imshow("detectLedsNoAoiNoTemplate: mask", temporalMask);
				//waitKey();
		}
#endif
			// Throw an error if less then a certain number of pixels are LED pixels 
			if (sum(temporalMask > 0)[0] < maskThr)
				throw AlgorithmError("sum(temporalMask > 0)[0] < maskThr");

			// Clean mask
			Mat clMask = cleanMask(temporalMask);
#ifdef DEBUG
			if (debug)
			{
				imshow("detectLedsNoAoiNoTemplate: clmask", clMask);
				//waitKey();
	}
#endif
			// extract led codes
			int total_frame_number = FramesStream.get(CV_CAP_PROP_FRAME_COUNT);
			for (int i = 0; i < total_frame_number; i++)
			{
				Mat frame;
				if (i < framesArr.size())
					frame = framesArr[i];
				else
					FramesStream >> frame;
#ifdef DEBUG
				if (debug)
					cout << i << " " << (i) * 100 / total_frame_number << "%" << endl;
#endif
				string code;
				try
				{
					code = detectLed(clMask, frame, optCols, portrait, hThr);
				}
				catch (AlgorithmError)
				{
					code = "u";
				}
				leds_codes.push_back(code);
			}

			return leds_codes;
}

		// extract leds code from the first frame
		int idx_frame_detect = 0;
		bool success_leds_detect = false;
		while (!success_leds_detect && idx_frame_detect < temporalMaskFramesNum)
		{
			//Mat mask = detectLedsTemporal(framesArr);
			string code;
			try
			{
				code = detectLedsNoAoiNoTemplate(framesArr, nExpLeds, optCols, idx_frame_detect, portrait, begPoint, endPoint, estPitch, rot, hThr, mask);
			}
			catch (AlgorithmError &ex)
			{
				for (int i = 0; i < nExpLeds; i++)
					code += "u";
			}
			int cnt = 0;
			for (auto s : code)
				cnt += int(s == 'u');
			if (cnt <= nExpLeds * 2 / 5)
				success_leds_detect = true;
			leds_codes.push_back(code);
			idx_frame_detect++;
		}

		// throw exception!!!
		//if temporal mask did not succeed on 7 initial frames - increase the number of frames and try again
		if (!success_leds_detect) {
			//throw AlgorithmError("extract frame code from first frame failed - increasing frames num");

			readNextFrames(FramesStream, temporalMaskFramesNum2, aoiSz, framesArr);
			Mat mask = detectLedsTemporal(framesArr);
			vector<int> hThr = calculateHueBasedThreshold(framesArr, mask); //Aleksey code even faster than my _fast code
			idx_frame_detect = 0;
		}

		while (!success_leds_detect && idx_frame_detect < temporalMaskFramesNum)
		{
			string code;
			try
			{
				code = detectLedsNoAoiNoTemplate(framesArr, nExpLeds, optCols, idx_frame_detect, portrait, begPoint, endPoint, estPitch, rot, hThr, mask);
			}
			catch (AlgorithmError &ex)
			{
				for (int i = 0; i < nExpLeds; i++)
					code += "u";
			}
			int cnt = 0;
			for (auto s : code)
				cnt += int(s == 'u');
			if (cnt <= nExpLeds * 2 / 5)
				success_leds_detect = true;
			leds_codes.push_back(code);
			idx_frame_detect++;
		}
		if (!success_leds_detect)
			throw AlgorithmError("Error - failed to find any LED data from first frames");

		//Continue to extract leds code from the next frames, when the leds approximate location is known
		//Looking for 7 consecutive frames in order to calculate the color threshold
		bool successConsecutive = false;
		int consecCount = 1;
		if (success_leds_detect)
		{
			while ((idx_frame_detect <= maxFirstFrameRead && !successConsecutive) || (idx_frame_detect < numMinimumRead))
			{
				if (idx_frame_detect < temporalMaskFramesNum)
				{
					//FramesStream >> frame;
				}
				else
				{
					FramesStream >> frame;
					framesArr.push_back(changeFrameSize(frame, aoiSz));
				}


				if (rot)
					rotate_image_90(framesArr[idx_frame_detect]);

				if (framesArr[idx_frame_detect].empty())
					break;
				string code = detedLedsInAoi(framesArr[idx_frame_detect], nExpLeds, optCols, portrait, begPoint, endPoint, estPitch, hThr);
				int cnt = 0;
				for (auto s : code)
					cnt += int(s == 'u');
				if (cnt * 5 < nExpLeds)
					consecCount += 1;
				if (consecCount == NumConsecutiveRequired)
					successConsecutive = true;
				idx_frame_detect++;
				leds_codes.push_back(code);
			}
		}
		std::cout << "7 first frames: " << std::fixed << timer << "ms\n";
		timer.Reset();

		/*		// extract leds code from the next frames that have already been read
				for (; idx_frame_detect < framesArr.size(); idx_frame_detect++)
				{
					if (rot)
						rotate_image_90(framesArr[idx_frame_detect]);
					string code = detedLedsInAoi(framesArr[idx_frame_detect], nExpLeds, optCols, portrait, begPoint, endPoint, estPitch, hThr);
					leds_codes.push_bac(code);
				}*/
				// extract leds code from the rest of the frames 
		cout << "extract from video" << endl;
		int cnt = 0;
		int total_frame_number = FramesStream.get(CV_CAP_PROP_FRAME_COUNT);
		while (true)
		{
#ifdef DEBUG
			if (debug)
				cout << ++cnt << " " << (idx_frame_detect + cnt) * 100 / total_frame_number << "%" << endl;
#endif
			Mat frame;
			FramesStream >> frame;
			if (frame.empty())
				break;
			frame = changeFrameSize(frame, aoiSz);
			if (rot)
				rotate_image_90(frame);
			//imshow("Non first frame rotated and pre-cropped to ROI", frame);
			//waitKey(10);
			string code = detedLedsInAoi(frame, nExpLeds, optCols, portrait, begPoint, endPoint, estPitch, hThr);
			leds_codes.push_back(code);
			idx_frame_detect++;

			//Omri - stop after 12 frames - to test algorithm speed (before Aleksey changes)
			if (idx_frame_detect == 12)
				break;

		}
		std::cout << "next frames: " << std::fixed << timer << "ms\n";
		return leds_codes;
	}
}
