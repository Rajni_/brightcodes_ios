//
//  SettingsViewController.swift
//  BrightCodes
//
//  Created by ksglobal on 06/08/18.
//  Copyright © 2018 ksglobal. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let arrTitle = ["White Balance","ISO","Exposure Time","FPS","Size","Focus distance","Zoom", "Show if recognized", "Log output into file", "Algorithm"]
    
    var arrWhiteBalance = ["auto","incandescent","fluorecent","warm-fluoresent","daylight","cloudy-daylight","twilight","shade"]
    
    var whiteBalanceString  = "daylight"
    
    var arrwhiteBalanceValues = [0,1500,3500,4500,5300,6000,7000,9500]
    var arriso = ["100","200","400","800","1600"]
    var arrisoValues = [100,200,400,800,1600]
    
    var iso = "100"
    
    var arrexposure = ["1/50000","1/25000","1/10000","1/5000","1/2500","1/1000","1/500","1/250","1/60","1/30","1/15","1/8"]
    
    var exposure = "1/2500"
   
    var arrExposureValues = [0.08,0.16,0.24,0.32,0.40,0.48,0.56,0.64,0.72,0.80,0.88,1.0]
    var arrfps = ["15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"]
    var fps = "30"
    
    var arrfpsValues = [15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
    
    var arrFocusDistance = ["1","2","3","4","5","6","7","8","9","10"]
    var arrfocusValues = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
    
    var focus = "2"
    
    var arrZoom = ["None","1.5x","2x"]
    var arrZoomValues = [1,1.5,2]
    
    var zoom = "2x"
    
    var arrSize = ["high","medium","low","cif352x288","vga640x480","hd1280x720","hd1920x1080","hd4K3840x2160","iFrame960x540","iFrame1280x720","640X480","480X360"]
    var size = "hd1920x1080"
    
    var recognize = "Browser"
    var log = "Off"
    var algo = "Off"
    override func viewDidLoad() {
        super.viewDidLoad()
        viewToLoad()
        // Do any additional setup after loading the view.
    }
    func viewToLoad(){
        tableView.dataSource    = self
        tableView.delegate      = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70
        
         if let recogniz = UserDefaults.standard.string(forKey: "recognize"){
            recognize = recogniz
        }
        if let logValue = UserDefaults.standard.string(forKey: "log"){
            log = logValue
        }
        if let algoValue = UserDefaults.standard.string(forKey: "algo"){
            algo = algoValue
        }
        
        if let whiteBalance_ = UserDefaults.standard.string(forKey: "whiteBalanceKey"){
            whiteBalanceString = whiteBalance_
        }
        if let iso_ = UserDefaults.standard.string(forKey: "isoKey"){
            iso = iso_
        }
        if let exposure_ = UserDefaults.standard.string(forKey: "exposureKey"){
            exposure = exposure_
        }
        if let zoom_ = UserDefaults.standard.string(forKey: "zoomKey"){
            zoom = zoom_
        }
        if let size_ = UserDefaults.standard.string(forKey: "sizeKey"){
            size = size_
        }
        if let focus_ = UserDefaults.standard.string(forKey: "focusKey"){
            focus = focus_
        }
        if let fps_ = UserDefaults.standard.string(forKey: "fpsKey"){
            fps = fps_
        }
        
    }
    
    func whiteBalance() {
        DPPickerManager.shared.showPicker(title: "White Balance", selected: whiteBalanceString, strings: arrWhiteBalance) { (value, index, cancel) in
            if !cancel {
                UserDefaults.standard.set(self.arrwhiteBalanceValues[index], forKey: "whiteBalance")
                
                  UserDefaults.standard.set(value, forKey: "whiteBalanceKey")
                self.whiteBalanceString = value!
                self.tableView.reloadData()
            }
        }
    }
    
    func setIso() {
        DPPickerManager.shared.showPicker(title: "ISO", selected: iso, strings: arriso) { (value, index, cancel) in
            if !cancel {
                UserDefaults.standard.set(self.arrisoValues[index], forKey: "iso")
                UserDefaults.standard.set(value, forKey: "isoKey")
                self.iso = value!
                self.tableView.reloadData()
            }
        }
    }
    
    func setExposure() {
        DPPickerManager.shared.showPicker(title: "Exposure", selected: exposure, strings: arrexposure) { (value, index, cancel) in
            if !cancel {
                UserDefaults.standard.set(self.arrExposureValues[index], forKey: "exposure")
                 UserDefaults.standard.set(value, forKey: "exposureKey")
                self.exposure = value!
                self.tableView.reloadData()
            }
        }
    }
    
    func setFPS() {
        DPPickerManager.shared.showPicker(title: "FPS", selected: fps, strings: arrfps) { (value, index, cancel) in
            if !cancel {
                UserDefaults.standard.set(self.arrfpsValues[index], forKey: "fps")
                UserDefaults.standard.set(value, forKey: "fpsKey")
                self.fps = value!
                self.tableView.reloadData()
            }
        }
    }
    
    func setFocus() {
        DPPickerManager.shared.showPicker(title: "Focus Distance", selected: focus, strings: arrFocusDistance) { (value, index, cancel) in
            if !cancel {
                UserDefaults.standard.set(self.arrfocusValues[index], forKey: "focus")
                UserDefaults.standard.set(value, forKey: "focusKey")
                self.focus = value!
                self.tableView.reloadData()
            }
        }
    }
    
    func setZoomScale() {
        DPPickerManager.shared.showPicker(title: "Zoom", selected: zoom, strings: arrZoom) { (value, index, cancel) in
            if !cancel {
                UserDefaults.standard.set(self.arrZoomValues[index], forKey: "zoom")
                UserDefaults.standard.set(value, forKey: "zoomKey")
                self.zoom = value!
                self.tableView.reloadData()
            }
        }
    }
    func setSizeResolution(){
        DPPickerManager.shared.showPicker(title: "Size", selected: size, strings: arrSize) { (value, index, cancel) in
            if !cancel {
                UserDefaults.standard.set(value, forKey: "sizeKey")
                self.size = value!
                self.tableView.reloadData()
            }
        }
    }
    func setRecogniser() {
        if recognize == "Browser"{
            recognize = "Code"
           UserDefaults.standard.set("Code", forKey: "recognize")
        }
        else{
            recognize = "Browser"
           UserDefaults.standard.set("Browser", forKey: "recognize")
        }
         self.tableView.reloadData()
    }
    func setLog() {
        if log == "On"{
            log = "Off"
            UserDefaults.standard.set("Off", forKey: "log")
        }
        else{
            log = "On"
            UserDefaults.standard.set("On", forKey: "log")
        }
         self.tableView.reloadData()
    }
    func setAlgo() {
        if algo == "Off"{
            algo = "On"
            UserDefaults.standard.set("On", forKey: "algo")
        }
        else{
             algo = "Off"
           UserDefaults.standard.set("Off", forKey: "algo")
        }
         self.tableView.reloadData()
    }
    
    @IBAction func backAc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SettingsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            whiteBalance()
        }
        else if indexPath.row == 1{
            setIso() 
        }
        else if indexPath.row == 2{
            setExposure()
        }
        else if indexPath.row == 3{
            setFPS()
        }
        else if indexPath.row == 4{
            setSizeResolution()
        }
        else if indexPath.row == 5{
            setFocus()
        }
        else if indexPath.row == 6{
            setZoomScale()
        }
        else if indexPath.row == 7{
            setRecogniser()
        }
        else if indexPath.row == 8{
            setLog()
        }
        else if indexPath.row == 9{
            setAlgo()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
extension SettingsViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! SettingCell
        cell.titleLabel.text = arrTitle[indexPath.row]
        if indexPath.row == 0{
            cell.descLab.text = whiteBalanceString
            cell.descLab.backgroundColor = UIColor.clear
        }
        else if indexPath.row == 1{
            cell.descLab.text = iso
            cell.descLab.backgroundColor = UIColor.clear
        }
        else if indexPath.row == 2{
            cell.descLab.text = exposure
            cell.descLab.backgroundColor = UIColor.clear
        }
        else if indexPath.row == 3{
            cell.descLab.text = fps
            cell.descLab.backgroundColor = UIColor.clear
        }
        else if indexPath.row == 4{
             cell.descLab.text = size
            cell.descLab.backgroundColor = UIColor.clear
        }
        else if indexPath.row == 5{
            cell.descLab.text = focus
            cell.descLab.backgroundColor = UIColor.clear
        }
        else if indexPath.row == 6{
            cell.descLab.text = zoom
            cell.descLab.backgroundColor = UIColor.clear
        }
        else if indexPath.row == 7{
             cell.descLab.text = "   \(recognize)   "
             cell.descLab.backgroundColor = UIColor.lightGray.withAlphaComponent(20)
        }
        else if indexPath.row == 8{
             cell.descLab.text =  "   \(log)   "
             cell.descLab.backgroundColor = UIColor.lightGray.withAlphaComponent(20)
        }
        else if indexPath.row == 9{
             cell.descLab.text = "   \(algo)   "
             cell.descLab.backgroundColor = UIColor.lightGray.withAlphaComponent(20)
        }
        
        return cell
    }
}

