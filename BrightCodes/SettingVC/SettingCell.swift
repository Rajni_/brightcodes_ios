//
//  SettingCell.swift
//  BrightCodes
//
//  Created by ksglobal on 08/08/18.
//  Copyright © 2018 ksglobal. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet weak var descLab: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
